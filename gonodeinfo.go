//
// gonodeinfo.go
//

package main

import "flag"
import "fmt"
import "io"
import "os"
import "os/exec"
import "encoding/json"
import "time"
import "strings"
import "log"
import "io/ioutil"
import "regexp"
import "net"

const KiB = 1024
const MiB = 1024 * 1024
const GiB = 1024 * 1024 * 1024

type JsonValue interface{}
type JsonObject map[string]JsonValue

var errors = flag.Bool("e", false, "report errors")
var verbose = flag.Bool("v", false, "report progress activity")
var print = flag.Bool("p", false, "print json data to stdout")
var no_send = flag.Bool("n", false, "do not send json data to servers")
var config_file = flag.String("c", "", "configuration file")

func main() {
	// Determine initial files or directories
	flag.Parse()
	roots := flag.Args()
	if len(roots) == 0 {
		roots = []string{"."}
	}

	config_file := find_config([]string{"gonodeinfo.conf", "/etc/gonodeinfo.conf"})

	if len(config_file) < 1 {
		log.Fatal("Cannot find a configuration file")
		// does not return
	}

	config := read_config(config_file)

	servers := config["Servers"]

	bad_config := false

	if len(servers) < 1 {
		log.Print("Bad configuration: no Servers")
		bad_config = true
	}

	if bad_config {
		log.Fatalf("Bad configuration file \"%s\"", config_file)
		// does not return
	}

	if *verbose {
		log.Printf("Configuration: servers: %s\n", servers)
	}

	//fmt.Printf("hostname: %s\n", read_program("hostname"));
	//fmt.Printf("/etc/hostname: %s\n", read_file("/etc/hostname"));
	//fmt.Printf("lsb_release: %s\n", read_program("lsb_release","-a"));
	//fmt.Printf("redhat-release: %s\n", read_file("/etc/redhat-release"));

	jdata := make(JsonObject)

	jdata["config"] = config

	jdata["hostname"] = read_program("hostname")
	jdata["etc_hostname"] = read_file("/etc/hostname")

	jdata["lsb_release"] = read_program("lsb_release", "-a")
	jdata["etc_redhat_release"] = read_file("/etc/redhat-release")
	jdata["etc_debian_version"] = read_file("/etc/debian_version")

	t := time.Now()

	jdata["time"] = t.Unix()
	jdata["time_now"] = t

	jdata["uname_a"] = read_program("uname", "-a")
	jdata["uname_r"] = read_program("uname", "-r")

	jdata["cpuinfo"] = read_file("/proc/cpuinfo")
	jdata["meminfo"] = read_file("/proc/meminfo")
	jdata["mounts"] = read_file("/proc/mounts")
	jdata["swaps"] = read_file("/proc/swaps")
	jdata["scsi"] = read_file("/proc/scsi/scsi")
	jdata["mdstat"] = read_file("/proc/mdstat")

	if file_exists("/sbin/lspci") {
		jdata["lspci"] = read_program("/sbin/lspci")
		jdata["lspci_tv"] = read_program("/sbin/lspci", "-tv")
		jdata["lspci_vv"] = read_program("/sbin/lspci", "-vv")
	} else if file_exists("/usr/bin/lspci") {
		jdata["lspci"] = read_program("/usr/bin/lspci")
		jdata["lspci_tv"] = read_program("/usr/bin/lspci", "-tv")
		jdata["lspci_vv"] = read_program("/usr/bin/lspci", "-vv")
	}

	jdata["dmidecode"] = read_program("/usr/sbin/dmidecode")

	jdata["uptime"] = read_program("uptime")

	jdata["etc_hosts"] = read_file("/etc/hosts")
	jdata["etc_resolv_conf"] = read_file("/etc/resolv.conf")

	domain_name := read_program("domainname")
	if *verbose {
		fmt.Fprintf(os.Stderr, "Check NIS domainname: \"%s\" len %d\n", domain_name, len(domain_name))
	}
	if domain_name == "(none)" {
		domain_name = ""
	}
	if len(domain_name) > 0 {
		jdata["yp_domainname"] = domain_name
		jdata["yp_etc_yp_conf"] = read_file("/etc/yp.conf")
		jdata["yp_ypwhich"] = read_program("ypwhich")
		jdata["yp_ypwhich_m"] = read_program("ypwhich", "-m")
		jdata["yp_ypserv"] = read_program("/sbin/chkconfig", "--list", "ypserv")
		if file_exists("/sbin/pidof") {
			jdata["yp_ypserv_pid"] = read_program("/sbin/pidof", "ypserv")
		} else if file_exists("/usr/sbin/pidof") {
			jdata["yp_ypserv_pid"] = read_program("/usr/sbin/pidof", "ypserv")
		} else if file_exists("/usr/bin/pidof") {
			jdata["yp_ypserv_pid"] = read_program("/usr/bin/pidof", "ypserv")
		}
		jdata["yp_securenets"] = read_file("/var/yp/securenets")
		jdata["yp_ypservers"] = read_file("/var/yp/ypservers")
	}

	jdata["ifconfig"] = read_program("/sbin/ifconfig", "-a")
	jdata["netstat_rn"] = read_program("netstat", "-rn")

	jdata["ip_a"] = read_program("/sbin/ip", "a")
	jdata["ip_r"] = read_program("/sbin/ip", "r")

	jdata["ethtool"] = encode_ethtool()

	jdata["iptables"] = read_program("iptables", "-L", "-v")

	jdata["chronyc_sources"] = read_program("chronyc", "sources")
	jdata["chronyc_tracking"] = read_program("chronyc", "tracking")

	jdata["timedatectl_show"] = read_program("timedatectl", "show")
	jdata["timedatectl_timesync_status"] = read_program("timedatectl", "timesync-status")

	jdata["ntpstat"] = read_program("/usr/bin/ntpstat")
	jdata["ntptrace"] = read_program("/usr/bin/ntptrace")

	jdata["root_dot_forward"] = read_file("/root/.forward")

	jdata["hosts_allow"] = read_file("/etc/hosts.allow")
	jdata["hosts_deny"] = read_file("/etc/hosts.deny")

	jdata["var_mail"] = read_program("/bin/ls", "-l", "/var/mail")
	jdata["var_spool_mail"] = read_program("/bin/ls", "-l", "/var/spool/mail")

	jdata["sys_block"] = encode_sys_block()

	jdata["sensors"] = read_program("/usr/bin/sensors")

	jdata["lscpu"] = read_program("lscpu")

	jdata["diskscrub_xml"] = read_file("/var/log/diskscrub.xml")

	jdata["rpms"] = read_program("/bin/rpm", "-qa")
	jdata["debs"] = read_program("/usr/bin/apt", "list", "--installed")

	jdata["stat_rpm_packages"] = read_program("stat", "/var/lib/rpm/Packages")
	jdata["stat_dpkg_status"] = read_program("stat", "/var/lib/dpkg/status")

	jdata["zpool_version"] = read_program("/sbin/zpool", "version")
	jdata["zpool_list"] = read_program("/sbin/zpool", "list", "-p")
	jdata["zpool_list_v"] = read_program("/sbin/zpool", "list", "-p", "-v")
	jdata["zpool_status"] = read_program("/sbin/zpool", "status")

	jdata["systemd_timers"] = read_program("/usr/bin/systemctl", "list-timers")
	jdata["systemd_unit_files"] = read_program("/usr/bin/systemctl", "list-unit-files")
	jdata["systemd_services"] = read_program("/usr/bin/systemctl")

	b, err := json.Marshal(jdata)
	if err != nil {
		fmt.Println("error:", err)
	}

	if *print {
		b, err := json.MarshalIndent(jdata, "", "  ")
		if err != nil {
			fmt.Println("error:", err)
		}
		os.Stdout.Write(b)
	}

	if !*no_send {
		servers_array := strings.Split(servers, " ")
		for _, v := range servers_array {
			func(s string) {
				if *verbose {
					fmt.Fprintf(os.Stderr, "Sending data to \"%s\"\n", s)
				}
				conn, err := net.Dial("tcp", s)
				if err != nil {
					log.Printf("Cannot send data to server \"%s\", net.Dial() error %v\n", s, err)
					return
				}
				count, write_err := conn.Write(b)
				//fmt.Printf("Write %d, count %d, error %v\n", len(b), count, write_err)
				if count != len(b) {
					log.Printf("Cannot send data to server \"%s\", Conn.Write() sent %d bytes out of %d, error %v\n", s, count, len(b), write_err)
				}
				if write_err != nil {
					log.Printf("Cannot send data to server \"%s\", Conn.Write() error %v\n", s, write_err)
				}
				close_err := conn.Close()
				if close_err != nil {
					log.Printf("Cannot send data to server \"%s\", Conn.Close() error %v\n", s, close_err)
				}
			}(v)
		}
	}
}

func encode_ethtool() JsonValue {
	jdata := make(JsonObject)

	ethls := read_program("/bin/ls", "-1", "/sys/class/net")
	eths := strings.Split(ethls, "\n")

	//jdata["ethls"] = ethls
	//jdata["eths"] = eths

	ethtool := ""

	if file_exists("/sbin/ethtool") {
		ethtool = "/sbin/ethtool"
	} else if file_exists("/usr/sbin/ethtool") {
		ethtool = "/usr/sbin/ethtool"
	} else {
		return jdata
	}

	for _, eth := range eths {
		x := make(JsonObject)
		x["ethtool"] = read_program(ethtool, eth)
		x["ethtool_P"] = read_program(ethtool, "-P", eth)
		x["ethtool_i"] = read_program(ethtool, "-i", eth)
		jdata[eth] = x
	}

	return jdata
}

func encode_sys_block() JsonValue {
	jdata := make(JsonObject)

	devls := read_program("/bin/ls", "-1", "/sys/block")
	devs := strings.Split(devls, "\n")

	//jdata["devls"] = devls
	//jdata["devs"] = devs

	for _, dev := range devs {
		if strings.HasPrefix(dev, "loop") {
			continue
		}
		x := make(JsonObject)
		x["size"] = read_file("/sys/block/" + dev + "/size")
		x["ro"] = read_file("/sys/block/" + dev + "/ro")
		x["removable"] = read_file("/sys/block/" + dev + "/removable")
		//x["vendor"] = read_file("/sys/block/"+dev+"/device/vendor")
		x["model"] = read_file("/sys/block/" + dev + "/device/model")
		x["slaves"] = read_program("/bin/ls", "-1", "/sys/block/"+dev+"/slaves")
		if !strings.HasPrefix(dev, "ram") && !strings.HasPrefix(dev, "loop") && !strings.HasPrefix(dev, "md") {
			x["smartctl"] = read_program("/usr/sbin/smartctl", "-a", "/dev/"+dev)
		}
		if strings.HasPrefix(dev, "md") {
			x["md_metadata_version"] = read_file("/sys/block/" + dev + "/md/metadata_version")
			x["md_array_size"] = read_file("/sys/block/" + dev + "/md/array_size")
			x["md_component_size"] = read_file("/sys/block/" + dev + "/md/component_size")
			x["md_array_state"] = read_file("/sys/block/" + dev + "/md/array_state")
			x["md_level"] = read_file("/sys/block/" + dev + "/md/level")
			x["md_degraded"] = read_file("/sys/block/" + dev + "/md/degraded")
			x["md_mismatch_cnt"] = read_file("/sys/block/" + dev + "/md/mismatch_cnt")
			x["md_bitmap_chunksize"] = read_file("/sys/block/" + dev + "/md/bitmap/chunksize")
		}
		jdata[dev] = x
	}

	return jdata
}

func chop(s string) string {
	return strings.TrimSuffix(s, "\n")
}

func read_pipe(path string, file *os.File) string {
	s := ""
	buf := make([]byte, KiB)
	for {
		count, err := file.Read(buf)
		if err == io.EOF {
			return s
		}
		if err != nil {
			if *errors {
				fmt.Fprintf(os.Stderr, "file.Read(%s) error %v\n", path, err)
			}
			return s
		}
		s += string(buf[:count])
	}
}

func read_file(path string) JsonValue {
	if *verbose {
		fmt.Fprintf(os.Stderr, "Reading %s\n", path)
	}
	file, err := os.Open(path)
	if err != nil {
		if *errors {
			fmt.Fprintf(os.Stderr, "os.Open(%s) error %v\n", path, err)
		}
		return nil
	}
	defer file.Close()
	return chop(read_pipe(path, file))
}

func read_program(program string, args ...string) string {
	if *verbose {
		fmt.Fprintf(os.Stderr, "Reading exec %s\n", program)
	}
	// there is no exec.Command() that takes an array of args
	c := exec.Command(program)
	// manually append the command arguments
	a := make([]string, 1+len(args))
	a[0] = c.Args[0]
	copy(a[1:], args)
	c.Args = a
	// run the program, capture the output
	buf, err := c.CombinedOutput()
	if err != nil {
		if *errors {
			fmt.Fprintf(os.Stderr, "os.exec.Command(%s).CombinedOutput() error %v\n", program, err)
		}
		//return nil;
	}
	return chop(string(buf))
}

func file_exists(file string) bool {
	_, err := os.Stat(file)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	log.Printf("file_exists(%s): os.Stat() error %v", file, err)
	return false
}

func find_config(config_list []string) string {
	if len(*config_file) > 0 { // use config file given by "-c go.conf"
		if !file_exists(*config_file) {
			log.Fatalf("find_config: Config file \"%s\" does not exist.", *config_file)
			// DOES NOT RETURN
		}
		if *verbose {
			log.Printf("find_config: Using config file \"%s\" specified by \"-c\"", *config_file)
		}
		return *config_file
	}

	for _, v := range config_list {
		if file_exists(v) {
			if *verbose {
				log.Printf("find_config: Found config file \"%s\"", v)
			}
			return v
		}
	}
	return ""
}

func read_config(file string) map[string]string {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Printf("read_config: ioutil.ReadFile(%s) error %v\n", file, err)
		return nil
	}

	c := make(map[string]string)

	d1 := strings.Split((string)(data), "\n")

	split_re := regexp.MustCompile("(.*?)\\s*:\\s*(.*)")

	for k, v := range d1 {
		if strings.HasPrefix(v, "#") {
			continue
		}
		if strings.HasPrefix(v, " ") {
			continue
		}
		if strings.HasPrefix(v, "\t") {
			continue
		}
		if len(v) < 1 {
			continue
		}

		d2 := split_re.FindStringSubmatch(v)

		if len(d2) != 3 {
			log.Fatalf("read_config(%s): Line %d, Invalid entry %s\n", file, k+1, v)
			// DOES NOT RETURN
		}

		//fmt.Printf("d1 [%s], [%s] [%s] d2: %v\n", d1, d2[1], d2[2], d2);

		c[d2[1]] = d2[2]
	}

	return c
}

// end
