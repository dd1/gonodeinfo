//
// gonodereport.go
//

package main

import "flag"
import "fmt"
import "io"
import "os"
import "os/exec"
import "encoding/json"
import "time"
import "strings"
import "regexp"
import "io/ioutil"
import "strconv"
import "log"
import "sort"

const KiB = 1024
const MiB = 1024 * 1024
const GiB = 1024 * 1024 * 1024

type JsonValue interface{}
type JsonObject map[string]JsonValue

var errors = flag.Bool("e", false, "report errors")
var verbose = flag.Bool("v", false, "report progress activity")
var config_file = flag.String("c", "", "configuration file")

type Data struct {
	Filename string
	Json     map[string]interface{}
	Hostname string
	Uptime   string
	Os       string
	CpuType  string
	Cpu      string
	MemTotal string
}

var xkeys []string
var x = make(map[string]Data)

type SortByHostname []string

func (a SortByHostname) Len() int           { return len(a) }
func (a SortByHostname) Less(i, j int) bool { return x[a[i]].Hostname < x[a[j]].Hostname }
func (a SortByHostname) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func process() {
	re_uptime := regexp.MustCompile("up (\\d+) days")

	re_description := regexp.MustCompile("Distributor ID:\\s+(.*)\\n")
	re_release := regexp.MustCompile("Release:\\s+(.*)\\n")
	re_ubuntu := regexp.MustCompile("Ubuntu")
	re_centos := regexp.MustCompile("CentOS")
	re_sl := regexp.MustCompile("Scientific Linux")

	re_cpu := regexp.MustCompile("model name	:\\s+(.*)\\n")
	re_processor := regexp.MustCompile("Processor\\s+:\\s+(.*)\\n")
	re_hardware := regexp.MustCompile("Hardware\\s+:\\s+(.*)\\n")
	re_intel := regexp.MustCompile("Intel")
	re_amd := regexp.MustCompile("AMD")
	re_arm := regexp.MustCompile("ARM")
	re_intelcore := regexp.MustCompile("(Intel\\(R\\) Core\\(TM\\))")
	re_intelcore2 := regexp.MustCompile("(Intel\\(R\\) Core\\(TM\\)\\s*2\\s*CPU\\s*)")
	re_intelcore2duo := regexp.MustCompile("(Intel\\(R\\) Core\\(TM\\)\\s*2\\s*Duo\\s*CPU\\s*)")
	re_intelcore2quad := regexp.MustCompile("(Intel\\(R\\) Core\\(TM\\)\\s*2\\s*Quad\\s*CPU\\s*)")
	re_intelxeon := regexp.MustCompile("(Intel\\(R\\) Xeon\\(R\\)\\s*CPU\\s*)")
	re_cpuat := regexp.MustCompile("(CPU @)")
	re_amd_athlon := regexp.MustCompile("AMD Athlon\\(tm\\)")
	re_dual_core_processor := regexp.MustCompile("Dual Core Processor")

	re_mem_total := regexp.MustCompile("MemTotal:\\s+(\\d+)")

	for k, v := range x {
		v.Hostname = js(v.Json["hostname"])
		v.Uptime = x1(re_uptime.FindStringSubmatch(js(v.Json["uptime"])))

		lsb_release := js(v.Json["lsb_release"])
		description := x1(re_description.FindStringSubmatch(lsb_release))
		release := x1(re_release.FindStringSubmatch(lsb_release))
		if re_ubuntu.MatchString(lsb_release) {
			v.Os = "U-" + release
		} else if re_centos.MatchString(lsb_release) {
			v.Os = "CentOS-" + release
		} else if re_sl.MatchString(lsb_release) {
			v.Os = "SL-" + release
		} else {
			v.Os = description + " " + release
		}

		cpu := x1(re_cpu.FindStringSubmatch(js(v.Json["cpuinfo"])))

		//fmt.Printf("key %s, cpu [%s]\n", k, cpu);

		if len(cpu) < 1 {
			cpu = x1(re_processor.FindStringSubmatch(js(v.Json["cpuinfo"])))
		}

		if re_intel.MatchString(cpu) {
			v.CpuType = "Intel"
			cpu = re_intelxeon.ReplaceAllString(cpu, "")
			cpu = re_intelcore2duo.ReplaceAllString(cpu, "")
			cpu = re_intelcore2quad.ReplaceAllString(cpu, "")
			cpu = re_intelcore2.ReplaceAllString(cpu, "")
			cpu = re_intelcore.ReplaceAllString(cpu, "")
			cpu = re_cpuat.ReplaceAllString(cpu, "")
			v.Cpu = cpu
		} else if re_amd.MatchString(cpu) {
			v.CpuType = "AMD"
			cpu = re_amd_athlon.ReplaceAllString(cpu, "Athlon")
			cpu = re_dual_core_processor.ReplaceAllString(cpu, "")
			v.Cpu = cpu
		} else if re_arm.MatchString(cpu) {
			v.CpuType = "ARM"
			v.Cpu = x1(re_hardware.FindStringSubmatch(js(v.Json["cpuinfo"])))
		} else {
			v.CpuType = cpu
			v.Cpu = cpu
		}

		memtotal := x1(re_mem_total.FindStringSubmatch(js(v.Json["meminfo"])))
		memtotal_int, _ := strconv.ParseInt(memtotal, 10, 0)
		v.MemTotal = fmt.Sprintf("%d", memtotal_int/KiB)

		x[k] = v
	}

	xkeys = make([]string, 0, len(x))
	for k, _ := range x {
		xkeys = append(xkeys, k)
	}
	sort.Stable(SortByHostname(xkeys))
}

func x1(s []string) string {
	if len(s) < 2 {
		return ""
	}
	return s[1]
}

func main() {
	// Determine initial files or directories
	flag.Parse()
	roots := flag.Args()
	if len(roots) == 0 {
		roots = []string{"."}
	}

	config_file := find_config([]string{"gonodereport.conf", "/etc/gonodereport.conf"})

	if len(config_file) < 1 {
		log.Fatal("Cannot find a configuration file")
		// does not return
	}

	config := read_config(config_file)

	data_dir := config["DataDir"]
	html_dir := config["HtmlDir"]

	bad_config := false

	if len(data_dir) < 1 {
		log.Print("Bad configuration: no DataDir")
		bad_config = true
	}

	if len(html_dir) < 1 {
		log.Print("Bad configuration: no HtmlDir")
		bad_config = true
	}

	if bad_config {
		log.Fatalf("Bad configuration file \"%s\"", config_file)
		// does not return
	}

	if *verbose {
		fmt.Printf("data_dir: %s, html_dir: %s\n", data_dir, html_dir)
	}

	read_data(data_dir)

	//read_json("armdaq05.json")

	process()

	h := ""

	h += t("a", "", a("name", "main")) + "\n"
	h += t("h2", "Overview") + "\n"
	h += overview_table()
	h += "\n"

	h += t("a", "", a("name", "main")) + "\n"
	h += t("h2", "gonodeinfo.conf") + "\n"
	h += gonodeinfo_conf_table()
	h += "\n"

	h += t("a", "", a("name", "main")) + "\n"
	h += t("h2", "DMI data") + "\n"
	h += dmidecode_table()
	h += "\n"

	html := ""
	html += "<!DOCTYPE html>\n"
	html += "<html>\n"
	html += "<head>\n"
	html += t("title", s("Cluster Configuration Page")) + "\n"
	html += t("script", "", a("type", "text/javascript"), a("src", "sorttable.js")) + "\n"
	html += "</head>\n"
	html += "<body>\n"
	html += t("h1", s("Cluster Configuration Page")) + "\n"
	html += t("p", "Last updated: "+time.Now().String()) + "\n"
	html += h
	html += "</body>\n"
	html += "</html>\n"

	file := html_dir + "/" + "gonodereport.html"

	tmp := file + ".tmp"
	os.Remove(tmp) // no check for error, if remove failed, WriteFile() will complain
	write_err := ioutil.WriteFile(tmp, ([]byte)(html), 0444)
	if write_err != nil {
		log.Fatalf("Cannot write file \"%s\", ioutil.WriteFile() error %v\n", tmp, write_err)
		// does not return
	}
	rename_err := os.Rename(tmp, file)
	if rename_err != nil {
		log.Fatalf("Cannot rename \"%s\" to \"%s\", os.Rename() error %v\n", tmp, file, rename_err)
		// does not return
	}
}

func overview_table() string {
	thead := ""
	thead += "<thead>\n"
	thead += t("th", "Hostname")
	thead += t("th", "Uptime")
	thead += t("th", "Type")
	thead += t("th", "CPU")
	thead += t("th", "RAM")
	thead += t("th", "swap")
	//thead += t("th", "Packages")
	thead += t("th", "Last update")
	thead += t("th", "OS")
	thead += t("th", "Kernel")
	thead += t("th", "Alarms")
	thead += t("th", "Description")
	thead += "</thead>\n"

	tbody := ""
	tbody += "<tbody>\n"

	for _, k := range xkeys {
		v := x[k]
		j := v.Json
		e := ""
		// hostname
		e += t("td", s(v.Hostname), a("align", "center"))
		// uptime
		e += t("td", s(v.Uptime), a("align", "center"))
		// type
		e += t("td", s(v.CpuType), a("align", "center"))
		// cpu
		e += t("td", s(v.Cpu), a("align", "center"))
		// ram
		e += t("td", s(v.MemTotal), a("align", "center"))
		// swap
		e += t("td", s(js(j["xxx"])), a("align", "center"))
		// last update
		e += t("td", s(js(j["xxx"])), a("align", "center"))
		// os
		e += t("td", s(v.Os), a("align", "center"))
		// kernel
		e += t("td", s(js(j["uname_r"])), a("align", "center"))
		// alarms
		e += t("td", s(js(j["xxx"])), a("align", "center"))
		// description
		e += t("td", s(js(jo(j["config"])["Description"])), a("align", "center"))

		tbody += t("tr", e) + "\n"
	}

	tbody += "</tbody>\n"

	return t("table", thead+tbody, a("border", "1"), a("align", "center"), a("class", "sortable"))
}

func gonodeinfo_conf_table() string {

	d := make(map[string]int)

	for _, v := range x {
		j := v.Json

		c := jo(j["config"])

		for cc, _ := range c {
			ccc := js(cc)
			d[ccc]++
		}
	}

	delete(d, "Description")

	headers := make([]string, 0, len(d))
	for k, _ := range d {
		headers = append(headers, k)
	}

	sort.Strings(headers)

	thead := ""
	thead += "<thead>\n"
	thead += t("th", "Hostname") + "\n"
	thead += t("th", "Description") + "\n"
	for _, hh := range headers {
		thead += t("th", s(hh)) + "\n"
	}
	thead += "</thead>\n"

	tbody := ""
	tbody += "<tbody>\n"

	for _, k := range xkeys {
		v := x[k]
		j := v.Json
		c := jo(j["config"])

		e := ""
		// hostname
		e += t("td", s(v.Hostname), a("align", "center"))
		e += t("td", s(js(c["Description"])), a("align", "center"))

		for _, hh := range headers {
			vv := js(c[hh])
			e += t("td", s(vv), a("align", "center")) + "\n"
		}

		tbody += t("tr", e) + "\n"
	}

	tbody += "</tbody>\n"

	return t("table", thead+tbody, a("border", "1"), a("align", "center"), a("class", "sortable"))
}

func dmidecode_table() string {

	thead := ""
	thead += "<thead>\n"
	thead += t("th", "Hostname")
	thead += t("th", "Motherboard")
	thead += t("th", "BIOS")
	thead += t("th", "RAM")
	thead += "</thead>\n"

	tbody := ""
	tbody += "<tbody>\n"

	for _, k := range xkeys {
		v := x[k]
		jdmi := js(v.Json["dmidecode"])

		jdmis := strings.Split(jdmi, "Handle 0x")

		mobo := ""
		bios := ""
		ram := ""

		for _, jh := range jdmis {
			//fmt.Fprintf(os.Stderr, "dmi %v\n", jh)
			if strings.Contains(jh, "Base Board Information") {
				// parse:
				// Manufacturer: Supermicro
				// Product Name: X9SCL/X9SCM
				// Version: 1.11A
				re_manufacturer := regexp.MustCompile("Manufacturer:\\s+(.*)\\n")
				re_product := regexp.MustCompile("Product Name:\\s+(.*)\\n")
				re_version := regexp.MustCompile("Version:\\s+(.*)\\n")

				mobo_manu := x1(re_manufacturer.FindStringSubmatch(jh))

				if strings.Contains(mobo_manu, "ASUS") {
					mobo_manu = "ASUS"
				}

				mobo += mobo_manu
				mobo += " "
				mobo += x1(re_product.FindStringSubmatch(jh))
				mobo += " "
				mobo += x1(re_version.FindStringSubmatch(jh))
				mobo += "<br>"
			} else if strings.Contains(jh, "BIOS Information") {
				// parse:
				// BIOS Information
				//     Vendor: American Megatrends Inc.
				//     Version: 1.1a
				//     Release Date: 09/28/2011
				//     Address: 0xF0000
				//     Runtime Size: 64 kB
				//     ROM Size: 8192 kB

				re_version := regexp.MustCompile("Version:\\s+(.*)\\n")

				bios += x1(re_version.FindStringSubmatch(jh))
				bios += "<br>"
			} else if strings.Contains(jh, "Memory Device") {
				// parse:
				// Memory Device
				//       Array Handle: 0x0027
				//       Error Information Handle: No Error
				//       Total Width: 72 bits
				//       Data Width: 64 bits
				//       Size: 4096 MB
				//       Form Factor: DIMM
				//       Set: None
				//       Locator: DIMM_2A
				//       Bank Locator: BANK1
				//       Type: DDR3
				//       Type Detail: Synchronous
				//       Speed: 1333 MHz
				//       Manufacturer: Kingston
				//       Serial Number: 32237C0D
				//       Asset Tag: A1_AssetTagNum1
				//       Part Number: 9965525-018.A00LF
				//       Rank: 2
				//       Configured Clock Speed: 32768 MHz

				re_size := regexp.MustCompile("Size:\\s+(.*)\\n")
				re_locator := regexp.MustCompile("Locator:\\s+(.*)\\n")
				re_type := regexp.MustCompile("Type:\\s+(.*)\\n")
				re_speed := regexp.MustCompile("Speed:\\s+(.*)\\n")
				re_actual_speed := regexp.MustCompile("Configured Clock Speed:\\s+(.*)\\n")

				xram := ""
				xram += x1(re_locator.FindStringSubmatch(jh))
				xram += " "
				xram += x1(re_size.FindStringSubmatch(jh))
				xram += " "
				xram += x1(re_type.FindStringSubmatch(jh))
				xram += " "
				xram += x1(re_speed.FindStringSubmatch(jh))
				xram += " "
				xram += x1(re_actual_speed.FindStringSubmatch(jh))

				if strings.Contains(jh, "Form Factor") {
					ram += xram
					ram += "<br>"
				}
			}
		}

		e := ""
		// hostname
		e += t("td", s(v.Hostname), a("align", "center"))
		// motherboard
		e += t("td", s(mobo), a("align", "center"))
		// bios
		e += t("td", s(bios), a("align", "center"))
		// ram
		e += t("td", s(ram), a("align", "center"))

		tbody += t("tr", e) + "\n"
	}

	tbody += "</tbody>\n"

	return t("table", thead+tbody, a("border", "1"), a("align", "center"), a("class", "sortable"))
}

func read_json(file string) {
	var ddd Data
	ddd.Filename = file
	var d interface{}
	b := read_file(file)
	err := json.Unmarshal(([]byte)(b), &d)
	if err != nil {
		return
	}
	dd, ok := d.(map[string]interface{})
	if !ok {
		return
	}
	ddd.Json = dd
	x[file] = ddd
}

func chop(s string) string {
	return strings.TrimSuffix(s, "\n")
}

func js(jv interface{}) string {
	s, ok := jv.(string)
	if !ok {
		return ""
	}
	return s
}

func jo(jv interface{}) map[string]interface{} {
	s, ok := jv.(map[string]interface{})
	if !ok {
		return make(map[string]interface{})
	}
	return s
}

func a(name, value string) string {
	return name + "='" + value + "'"
}

func t(tag, data string, attrs ...string) string {
	a := ""
	if len(attrs) > 0 {
		for _, aa := range attrs {
			a += " "
			a += aa
		}
	}
	return "<" + tag + a + ">" + data + "</" + tag + ">"
}

func s(s string) string {
	return s
}

func read_pipe(path string, file *os.File) string {
	s := ""
	buf := make([]byte, KiB)
	for {
		count, err := file.Read(buf)
		if err == io.EOF {
			return s
		}
		if err != nil {
			if *errors {
				fmt.Fprintf(os.Stderr, "file.Read(%s) error %v\n", path, err)
			}
			return s
		}
		s += string(buf[:count])
	}
}

func read_data(dir string) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatalf("read_data: ioutil.ReadDir(%s) error %v", dir, err)
		// DOES NOT RETURN
	}

	for _, f := range files {
		if !strings.HasSuffix(f.Name(), ".json") {
			continue
		}
		read_json(dir + "/" + f.Name())
	}
}

func read_file(path string) string {
	if *verbose {
		fmt.Fprintf(os.Stderr, "Reading %s\n", path)
	}
	file, err := os.Open(path)
	if err != nil {
		if *errors {
			fmt.Fprintf(os.Stderr, "os.Open(%s) error %v\n", path, err)
		}
		return ""
	}
	defer file.Close()
	return chop(read_pipe(path, file))
}

func read_program(program string, args ...string) string {
	if *verbose {
		fmt.Fprintf(os.Stderr, "Reading exec %s\n", program)
	}
	// there is no exec.Command() that takes an array of args
	c := exec.Command(program)
	// manually append the command arguments
	a := make([]string, 1+len(args))
	a[0] = c.Args[0]
	copy(a[1:], args)
	c.Args = a
	// run the program, capture the output
	buf, err := c.CombinedOutput()
	if err != nil {
		if *errors {
			fmt.Fprintf(os.Stderr, "os.exec.Command(%s).CombinedOutput() error %v\n", program, err)
		}
		//return nil;
	}
	return chop(string(buf))
}

func file_exists(file string) bool {
	_, err := os.Stat(file)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	log.Printf("file_exists(%s): os.Stat() error %v", file, err)
	return false
}

func find_config(config_list []string) string {
	if len(*config_file) > 0 { // use config file given by "-c go.conf"
		if !file_exists(*config_file) {
			log.Fatalf("find_config: Config file \"%s\" does not exist.", *config_file)
			// DOES NOT RETURN
		}
		if *verbose {
			log.Printf("find_config: Using config file \"%s\" specified by \"-c\"", *config_file)
		}
		return *config_file
	}

	for _, v := range config_list {
		if file_exists(v) {
			if *verbose {
				log.Printf("find_config: Found config file \"%s\"", v)
			}
			return v
		}
	}
	return ""
}

func read_config(file string) map[string]string {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Printf("read_config: ioutil.ReadFile(%s) error %v\n", file, err)
		return nil
	}

	c := make(map[string]string)

	d1 := strings.Split((string)(data), "\n")

	split_re := regexp.MustCompile("(.*?)\\s*:\\s*(.*)")

	for k, v := range d1 {
		if strings.HasPrefix(v, "#") {
			continue
		}
		if strings.HasPrefix(v, " ") {
			continue
		}
		if strings.HasPrefix(v, "\t") {
			continue
		}
		if len(v) < 1 {
			continue
		}

		d2 := split_re.FindStringSubmatch(v)

		if len(d2) != 3 {
			log.Fatalf("read_config(%s): Line %d, Invalid entry %s\n", file, k+1, v)
			// DOES NOT RETURN
		}

		//fmt.Printf("d1 [%s], [%s] [%s] d2: %v\n", d1, d2[1], d2[2], d2);

		c[d2[1]] = d2[2]
	}

	return c
}

// end
