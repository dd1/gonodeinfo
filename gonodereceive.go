//
// gonodereceive.go
//

package main

import "flag"
import "os"
import "time"
import "net"
import "strings"
import "regexp"
import "io/ioutil"
import "strconv"
import "log"
import "io"
import "encoding/json"

const KiB = 1024
const MiB = 1024 * 1024
const GiB = 1024 * 1024 * 1024

var errors = flag.Bool("e", false, "report errors")
var verbose = flag.Bool("v", false, "report progress activity")
var config_file = flag.String("c", "", "configuration file")
var add_client = flag.String("a", "", "client hostname")
var bind_fail_ok = flag.Bool("b", false, "exit quitly on bind() error (for use in crontab)")

func x1(s []string) string {
	if len(s) < 2 {
		return ""
	}
	return s[1]
}

func main() {
	// Determine initial files or directories
	flag.Parse()
	roots := flag.Args()
	if len(roots) == 0 {
		roots = []string{"."}
	}

	config_file := find_config([]string{"gonodereport.conf", "/etc/gonodereport.conf"})

	if len(config_file) < 1 {
		log.Fatal("Cannot find a configuration file")
		// does not return
	}

	config := read_config(config_file)

	data_dir := config["DataDir"]
	tcp_port, tcp_port_err := strconv.Atoi(config["ListenPort"])
	timeout_sec, timeout_err := strconv.Atoi(config["TimeoutSec"])
	max_size_MiB, max_size_err := strconv.Atoi(config["MaxSizeMiB"])

	bad_config := false

	if len(data_dir) < 1 {
		log.Print("Bad configuration: no DataDir")
		bad_config = true
	}

	if tcp_port_err != nil {
		log.Print("Bad configuration: missing or bad ListenPort, strconv.Atoi() error %v", tcp_port_err)
		bad_config = true
	}

	if timeout_err != nil {
		log.Printf("Bad configuration: missing or bad TimeoutSec, strconv.Atoi() error %v", timeout_err)
		bad_config = true
	}

	if max_size_err != nil {
		log.Printf("Bad configuration: missing or bad MaxSizeMiB, strconv.Atoi() error %v", max_size_err)
		bad_config = true
	}

	if bad_config {
		log.Fatalf("Bad configuration file \"%s\"", config_file)
		// does not return
	}

	if *verbose {
		log.Printf("Configuration: data_dir: %s, tcp_port: %d, max_size %d MiB\n", data_dir, tcp_port, max_size_MiB)
	}

	if len(*add_client) > 0 {
		ip_list, lookup_err := net.LookupIP(*add_client)
		if lookup_err != nil {
			log.Fatalf("Cannot find IP address for client \"%s\", net.LookupIP() error %v\n", *add_client, lookup_err)
			// does not return
		}

		for _, v := range ip_list {
			ip_addr := v.String()
			file := data_dir + "/" + ip_addr + ".json"
			if file_exists(file) {
				log.Printf("Client \"%s\" already exists, JSON file \"%s\"\n", *add_client, file)
				continue
			}
			if true || *verbose {
				log.Printf("Creating \"%s\"\n", file)
			}
			buf := make([]byte, 0, 0)
			write_err := ioutil.WriteFile(file, buf, 0444)
			if write_err != nil {
				log.Printf("Cannot create file \"%s\", ioutil.WriteFile() error %v\n", file, write_err)
				continue
			}
		}
		return
	}

	la := new(net.TCPAddr)
	la.Port = 8601
	listener, err := net.ListenTCP("tcp", la)
	if err != nil {
		s := err.Error()
		b := strings.Contains(s, "address already in use")
		if b && *bind_fail_ok {
			return
		}
		log.Fatalf("net.ListenTCP(%v) error %v", la, err)
		// DOES NOT RETURN
	}

	ch := make(chan *net.TCPConn)

	i := 0
	go func() {
		for {
			client, err := listener.AcceptTCP()
			if err != nil {
				log.Fatalf("listener.AcceptTCP() error %v", err)
				// DOES NOT RETURN
			}
			i++
			log.Printf("Connection %d from %v\n", i, client.RemoteAddr())
			ch <- client
		}
	}()

	for {
		client := <-ch
		go func(client *net.TCPConn) {
			addr, ok := (client.RemoteAddr()).(*net.TCPAddr)
			if !ok {
				log.Printf("Strange connection %v type %T, remote addr %T\n", client, client, client.RemoteAddr())
				client.Close()
				return
			}
			ip_addr := addr.IP.String()
			file := data_dir + "/" + ip_addr + ".json"
			if !file_exists(file) {
				log.Printf("Connection from %s rejected, to enable it, run \"touch %s\"\n", ip_addr, file)
				client.Close()
				return
			}
			if *verbose {
				log.Printf("Reading from %s\n", ip_addr)
			}
			client.SetReadDeadline(time.Now().Add((time.Duration)(timeout_sec) * time.Second))
			max_size := max_size_MiB * MiB
			buf := make([]byte, max_size_MiB*MiB)
			total := 0
			for {
				count, read_err := client.Read(buf[total:])
				total += count
				if *verbose {
					log.Printf("Reading from %v, got %d, total %d, error %v\n", client.RemoteAddr(), count, total, read_err)
				}
				if read_err != nil {
					if read_err != io.EOF {
						log.Printf("Reading from %v, got %d, total %d, error %v\n", client.RemoteAddr(), count, total, read_err)
					}
					break
				}

				if total >= max_size {
					log.Printf("Reading from %v, too much data, maximum is %d bytes\n", client.RemoteAddr(), max_size)
					break
				}
			}
			if *verbose {
				log.Printf("Reading from %v, total %d of %d\n", client.RemoteAddr(), total, len(buf))
			}
			client.Close()

			var json_data map[string]interface{}
			var json_err = json.Unmarshal(buf[0:total], &json_data)
			if json_err != nil {
				log.Printf("Reading from %v, cannot parse json data, json.Unmarshal() error %v\n", client.RemoteAddr(), json_err)
				return
			}

			var hostname = json_data["hostname"].(string)
			//log.Printf("Reading from %v hostname is \"%s\"\n", client.RemoteAddr(), hostname)

			if strings.Contains(hostname, "/") {
				log.Printf("Reading from %v, invalid hostname \"%s\" contains a slash\n", client.RemoteAddr(), hostname)
				return
			}

			if strings.Contains(hostname, "..") {
				log.Printf("Reading from %v, invalid hostname \"%s\" contains \"..\"\n", client.RemoteAddr(), hostname)
				return
			}

			save_file := data_dir + "/" + ip_addr + "." + hostname + ".json"

			tmp := save_file + ".tmp"
			os.Remove(tmp) // no check for error, if remove failed, WriteFile() will complain
			write_err := ioutil.WriteFile(tmp, buf[0:total], 0444)
			if write_err != nil {
				log.Printf("Reading from %v, cannot write file \"%s\", ioutil.WriteFile() error %v\n", client.RemoteAddr(), tmp, write_err)
				return
			}
			rename_err := os.Rename(tmp, save_file)
			if rename_err != nil {
				log.Printf("Reading from %v, cannot rename \"%s\" to \"%s\", os.Rename() error %v\n", client.RemoteAddr(), tmp, save_file, rename_err)
				return
			}
		}(client)
	}

	//ioutil.WriteFile(html_dir+"/"+"gonodereport.html", ([]byte)(html), 0444)
}

func file_exists(file string) bool {
	_, err := os.Stat(file)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	log.Printf("file_exists(%s): os.Stat() error %v", file, err)
	return false
}

func find_config(config_list []string) string {
	if len(*config_file) > 0 { // use config file given by "-c go.conf"
		if !file_exists(*config_file) {
			log.Fatalf("find_config: Config file \"%s\" does not exist.", *config_file)
			// DOES NOT RETURN
		}
		if *verbose {
			log.Printf("find_config: Using config file \"%s\" specified by \"-c\"", *config_file)
		}
		return *config_file
	}

	for _, v := range config_list {
		if file_exists(v) {
			if *verbose {
				log.Printf("find_config: Found config file \"%s\"", v)
			}
			return v
		}
	}
	return ""
}

func read_config(file string) map[string]string {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Printf("read_config: ioutil.ReadFile(%s) error %v\n", file, err)
		return nil
	}

	c := make(map[string]string)

	d1 := strings.Split((string)(data), "\n")

	split_re := regexp.MustCompile("(.*?)\\s*:\\s*(.*)")

	for k, v := range d1 {
		if strings.HasPrefix(v, "#") {
			continue
		}
		if strings.HasPrefix(v, " ") {
			continue
		}
		if strings.HasPrefix(v, "\t") {
			continue
		}
		if len(v) < 1 {
			continue
		}

		d2 := split_re.FindStringSubmatch(v)

		if len(d2) != 3 {
			log.Fatalf("read_config(%s): Line %d, Invalid entry %s\n", file, k+1, v)
			// DOES NOT RETURN
		}

		//fmt.Printf("d1 [%s], [%s] [%s] d2: %v\n", d1, d2[1], d2[2], d2);

		c[d2[1]] = d2[2]
	}

	return c
}

// end
