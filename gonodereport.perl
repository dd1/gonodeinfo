#!/usr/bin/perl -w
#
# Ubuntu LTS 18: apt install libjson-perl
# Ubuntu LTS 20: apt install libjson-perl
#

$| = 1;

#
# Command line switches
#

my $verbose = grep(/-v/, @ARGV);
my $help    = grep(/-h/, @ARGV) || grep(/--help/, @ARGV);
my $noemail = grep(/--noemail/, @ARGV); 
my $email   = grep(/--email/, @ARGV);
 
die "Usage: $0 [-h] [-v] [--email] [--noemail] [gonodereport.conf]\n" if $help; 

#
# Find gonodereport.conf
#

my ($confConf)  = grep(/(\w+\.conf)/, @ARGV);

$confConf = "/etc/gonodereport.conf" if ! $confConf;

die "Cannot read config file: $confConf: $!\n" unless -r $confConf;

foreach my $line (split(/\n/, `cat $confConf`)) {
  next if $line =~ /^#/; # comment
  next if $line =~ /^\s*$/; # empty line
  my ($tag, $value) = $line =~ /^(\w+.*?)\s*: *(.+)$/;
  $tag = lc($tag);
  print "$line, [$tag] [$value]\n" if $verbose;
  $conf{$tag} = $value;
}

#die "Here!";

# figure out groups of machines

my $groups = $conf{"groups"};

die "groups is not defined in config file $confConf\n" if ! $groups;

my @groups = split(/\s+/, $groups);
my %group_def;

for my $g (sort @groups) {
  my $gg = lc("group ".$g);
  my $def = $conf{$gg};
  die "$gg is not defined in config file $confConf\n" if ! $def;
  print "group [$g] [$gg] [$def]\n" if $verbose;
  $group_def{$g} = $def;
}

#die "Here!";

my $confNodes      = $conf{"xmldatadir"};
my $confGonodeinfo = $conf{"datadir"};
my $confWeb        = $conf{"htmldir"};

#die "xmldatadir is not defined in config file $confConf" if ! $confNodes;
die "datadir is not defined in config file $confConf" if ! $confGonodeinfo;
die "htmldir is not defined in config file $confConf" if ! $confWeb;

#die "$confNodes does not exist: $!" if ! -d $confNodes;
die "$confGonodeinfo does not exist: $!" if ! -d $confGonodeinfo;
die "$confWeb   does not exist: $!" if ! -d $confWeb;

mkdir $confWeb."/byOS",0777;
mkdir $confWeb."/byHost",0777;
mkdir $confWeb."/disks",0777;

system "rm -f $confWeb/byOS/*.txt";
system "rm -f $confWeb/byHost/*.txt";

# use bytes: If xml data files have garbage characters (i.e. from
# dmidecode), perl would try to interpret them as Unicode and would
# get confused. Tell it to not do Unicode. See man perlunicode.
#
use bytes;

my %nodedata;

my @descriptionheaders;

my %stale;

my %email;

my %class;
my %classbg;
my %classmain;

#my $xgray = "#808080";
my $xgray   = "#C0C0C0";
my $xred    = "red";
my $xyellow = "yellow";

my $c = 1;

my %short_hostname;

if ($confNodes && -d $confNodes) {
  my $files = `ls -1tr $confNodes/*.xml`;
  foreach my $f (split(/\n/,$files)) {
    my $data = readFile($f);
    my $hostname = findXmlTag($data,"hostname");
    my $short_hostname = $hostname;
    #print "File [$f] hostname [$hostname]\n";
    #if ($nodedata{$short_hostname}) {
    #  print "DUPE!\n";
    #}
    #$short_hostname =~ s/\..+$// unless $short_hostname =~ /^\d+/;
    $short_hostname =~ s/.triumf.ca//;
    $short_hostname =~ s/.localdomain//;
    $short_hostname{$hostname} = $short_hostname;
    $nodedata{$short_hostname} = $data;
  }
}
  
#use JSON::Parse 'parse_json';
use JSON 'decode_json';

my %jsondata;

my $gofiles = `ls -1tr $confGonodeinfo/*.json`;
foreach my $f (split(/\n/,$gofiles)) {
  my $data = readFile($f);
  next unless length($data) > 1;
  my $json = decode_json($data);
  if (0) {
    print "File [$f] [$json]\n";
    foreach my $k (sort keys %{$json}) {
      my $v = $$json{$k};
      print "json [$k]\n";
      #print "json [$k] [$v]\n";
    }
    #die "Done!";
  }
  my $hostname = $$json{"hostname"};
  my $short_hostname = $hostname;
  #$short_hostname =~ s/\..+$// unless $short_hostname =~ /^\d+/;
  $short_hostname =~ s/.triumf.ca//;
  $short_hostname =~ s/.localdomain//;
  #if ($jsondata{$short_hostname}) {
  #  print "DUPE!\n";
  #}
  $short_hostname{$hostname} = $short_hostname;
  $jsondata{$short_hostname} = $json;
  #my $xhostname = $jsondata{$short_hostname}{"hostname"};
  #print "File [$f] [$json] [$short_hostname] [$xhostname]\n";
}

#die "Here!";

# populate jsondata from XML data

my $now = time();

foreach my $h (sort keys %nodedata) {
  my $data = $nodedata{$h};
  my $xmltime = findXmlTag($data,"time");
  my $jsontime = $jsondata{$h}{"time"};

  # check if JSON data is newer than XML data

  if ($jsontime) {
    my $xmlage = $now - $xmltime;
    my $jsonage = $now - $jsontime;
    my $xmldays = int($xmlage/(24*60*60));
    my $jsondays = int($jsonage/(24*60*60));
    if ($jsondays < $xmldays + 2) {
      next;
    }
    #print "[$h] [$xmltime] [$jsontime] [$xmlage] [$jsonage] [$xmldays] [$jsondays]\n";
  }

  # XML data is newer, fill JSON data from XML

  #print "[$h] filling JSON data from XML\n";

  delete $jsondata{$h};

  $jsondata{$h}{"time"} = $xmltime;
  $jsondata{$h}{"hostname"} = findXmlTag($data, "hostname");
  $jsondata{$h}{"redhat"} = findXmlTag($data, "redhat");
  $jsondata{$h}{"uname"} = findXmlTag($data, "uname");
  $jsondata{$h}{"mounts"} = findXmlTag($data, "mounts");
  $jsondata{$h}{"swaps"} = findXmlTag($data, "swaps");
  $jsondata{$h}{"xml_etcnodeinfo"} = findXmlTag($data, "etcnodeinfo");
  $jsondata{$h}{"uptime"} = findXmlTag($data, "uptime");
  $jsondata{$h}{"cpuinfo"} = findXmlTag($data, "cpuinfo");
  $jsondata{$h}{"meminfo"} = findXmlTag($data, "meminfo");
  $jsondata{$h}{"xml_linux"} = findXmlTag($data, "linux");
  $jsondata{$h}{"xml_yum_updated"} = findXmlTag($data, "yum_updated");
  $jsondata{$h}{"rpms"} = findXmlTag($data, "rpms");
  $jsondata{$h}{"dmidecode"} = findXmlTag($data, "dmidecode");
  $jsondata{$h}{"lspci"} = findXmlTag($data, "lspci");
  $jsondata{$h}{"xml_ide"} = findXmlTag($data, "ide");
  $jsondata{$h}{"scsi"} = findXmlTag($data, "scsi");
  $jsondata{$h}{"mdstat"} = findXmlTag($data, "mdstat");
  $jsondata{$h}{"yp_domainname"} = findXmlTag($data, "domainname");
  $jsondata{$h}{"yp_ypserv_pid"} = findXmlTag($data, "ypserv");
  $jsondata{$h}{"yp_ypwhich"} = findXmlTag($data, "ypwhich");
  $jsondata{$h}{"yp_etc_yp_conf"} = findXmlTag($data, "etcypconf");
  $jsondata{$h}{"yp_securenets"} = findXmlTag($data, "securenets");
  $jsondata{$h}{"xml_securenets"} = findXmlTag($data, "ethtool");
  $jsondata{$h}{"xml_miitool"} = findXmlTag($data, "miitool");
  $jsondata{$h}{"xml_ifcfg"} = findXmlTag($data, "ifcfg");
  $jsondata{$h}{"xml_ifcfgeth0"} = findXmlTag($data, "ifcfgeth0");
  $jsondata{$h}{"etc_resolv_conf"} = findXmlTag($data, "etcresolvconf");
  $jsondata{$h}{"ifconfig"} = findXmlTag($data, "ifconfig");
  $jsondata{$h}{"xml_printcap"} = findXmlTag($data, "printcap");
  $jsondata{$h}{"ntpstat"} = findXmlTag($data, "ntpstat");
  $jsondata{$h}{"ntptrace"} = findXmlTag($data, "ntp");
  $jsondata{$h}{"chronyc_sources"} = findXmlTag($data, "chronysources");
  $jsondata{$h}{"chronyc_tracking"} = findXmlTag($data, "chronytracking");
  $jsondata{$h}{"root_dot_forward"} = findXmlTag($data, "rootforward");
  $jsondata{$h}{"var_spool_mail"} = findXmlTag($data, "mailspool");
  $jsondata{$h}{"xml_smart"} = findXmlTag($data, "smart");
  $jsondata{$h}{"diskscrub_xml"} = findXmlTag($data, "diskscrubxml");
  $jsondata{$h}{"xml_md"} = findXmlTag($data, "md");
  $jsondata{$h}{"xml_boot"} = findXmlTag($data, "boot");
  #$jsondata{$h}{"xml_rpmnew"} = findXmlTag($data, "rpmnew");
  #$jsondata{$h}{"xml_rpmsave"} = findXmlTag($data, "rpmsave");
  $jsondata{$h}{"xml_yum"} = findXmlTag($data, "yum");
  #$jsondata{$h}{"xml_chkconfig"} = findXmlTag($data, "chkconfig");
}

my %os;
my %rpmdata;
my %osdata;

foreach my $h (sort keys %jsondata) {
  my $hostname = $jsondata{$h}{"hostname"};
  #$hostname =~ s/\..+$// unless $hostname =~ /^\d+/;
  $hostname = $short_hostname{$hostname};

  $class{$h} = "style".$c++;

  my $time = $jsondata{$h}{"time"};
  if (time() - $time > 24*60*60) {
    $stale{$hostname} = 1;
      
    $classbg{$h} = $xgray;
    $classmain{$h} = $xgray;
  }

  my $redhat = $jsondata{$h}{"redhat"};

  if ($redhat) {
    #print "[$h] [$lsb_release] [$release] [$redhat]\n";
    $redhat =~ s/Red Hat Linux release/RHL /;
    $redhat =~ s/Red Hat Enterprise Linux release/RHEL /;
    #$redhat =~ s/Red Hat Enterprise Linux Server release/RHEL /;
    $redhat =~ s/Fedora Core release/FC /;
    $redhat =~ s/Fedora release/F /;
    $redhat =~ s/Scientific Linux SL Release/SL /;
    $redhat =~ s/Scientific Linux SL release/SL /;
    $redhat =~ s/Scientific Linux release/SL /;
    $redhat =~ s/CentOS release/CentOS /;
    $redhat =~ s/CentOS Linux release/CentOS /;
    $redhat =~ s/Red Hat Enterprise Linux WS release/RHEL WS /;
    $redhat =~ s/Scientific Linux CERN SLC release/SLC /;
    $redhat =~ s/ \(.*\)//;
    $redhat =~ s/  / /g;
    $redhat =~ s/ /-/g;
  } else {
    my $lsb_release = $jsondata{$h}{"lsb_release"};
    my ($description) = ($lsb_release =~ m/Distributor ID:\s+(.*)\n/m);
    my ($release) = ($lsb_release =~ m/Release:\s+(.*)\n/m);
    if ($lsb_release =~ m/Ubuntu/) {
      $redhat = "U-" . $release;
    } elsif ($lsb_release =~ m/Debian/) {
      $redhat = "D-" . $release;
    } elsif ($lsb_release =~ m/Raspbian/) {
      $redhat = "R-" . $release;
    } elsif ($lsb_release =~ m/CentOS/) {
      $redhat = "C-" . $release;
    } elsif ($lsb_release =~ m/Scientific Linux/) {
      $redhat = "SL-" . $release;
    } elsif ($lsb_release =~ m/Red Hat Enterprise Linux/) {
      $redhat = "RHEL-" . $release;
    } else {
      $redhat = $description . " " . $release;
    }
    #print "[$h] [$lsb_release] [$release] [$redhat]\n";
    #die "Here!";
  }
    
  my $uname    = $jsondata{$h}{"uname"};
  if (! $uname) {
    $uname     = $jsondata{$h}{"uname_a"};
  }
  
  #$redhat .= "-64" if $uname && $uname =~ /x86_64/;
  $redhat .= "-32" if $uname && $uname =~ /i686/;
  $redhat .= "-ARM" if $uname && $uname =~ /\sarm/;
 #if ($redhat =~ /SL/) {
  #  print "$redhat $uname\n";
  #}
  
  $os{$h} = $redhat;

  my $description = $jsondata{$h}{"config"}{"Description"};
  my $etcnodeinfo = $jsondata{$h}{"xml_etcnodeinfo"};
  if ($etcnodeinfo) {
    if (defined $etcnodeinfo) {
      if ($etcnodeinfo =~ /^(.*)\n?/) {
        $description = $1;
      }
      
      # discover all headers
      my @xxx = split(/\n/,$etcnodeinfo);
      # skip the first "description" line
      shift @xxx;
      foreach $hdr (@xxx) {
        if ($hdr =~ /(.*?)\s*:\s*.*/) {
          my $hhh = $1;
          if (! defined $headers{$hhh}) {
            push @descriptionheaders,$hhh;
            $headers{$hhh} = 1;
            
            #print "$hostname [$hhh] [$hdr]\n";
          }
        }
      }
    }
  }

  $description{$hostname} = $description;
}
  
foreach my $h (sort keys %jsondata) {
  my $rpms = $jsondata{$h}{"rpms"};
  next unless $rpms;
  $rpms =~ s/^\n//s;
  $rpms =~ s/\n$//s;
  $jsondata{$h}{"split_rpms"} = makeref(split(/\n/,$rpms));
}

foreach my $h (sort keys %jsondata) {
  my $debs = $jsondata{$h}{"debs"};
  next unless $debs;
  next if $debs =~ m/apt\: invalid flag\: list/;
  $debs =~ s/^\n//s;
  $debs =~ s/\n$//s;
  my @debs = split(/\n/,$debs);
  @debs = grep(!/^Listing/, @debs);
  @debs = grep(!/^WARNING/, @debs);
  @debs = grep(!/^$/, @debs);

  my @xdebs;
  foreach my $deb (sort @debs) {
    #$deb =~ s/installed//;
    #$deb =~ s/automatic//;
    #$deb =~ s/local//;
    #$deb =~ s/,upgradable to: (.*)]/]/;
    $deb =~ s/\[.*\]//;
    $deb =~ s/\[\]//;
    $deb =~ s/\s+$//;
    $deb =~ s/\/(.*),now//;
    $deb =~ s/\/now//;
    $deb =~ s/\[,local\]//;
    #die "[$deb]\n";
    push @xdebs, $deb;
  }

  $jsondata{$h}{"split_rpms"} = makeref(@xdebs);
}

foreach my $h (sort keys %jsondata)
  {
    my $os = $os{$h};
    push @{$osdata{$os}{"hosts"}},$h;
    foreach my $rpm (@{$jsondata{$h}{"rpms"}})
      {
	push @{$rpmdata{$rpm}{"hosts"}},$h;
	push @{$osdata{$os}{"rpms"}},$rpm;
      }
  }

foreach my $h (sort keys %jsondata) {
  my $time = $jsondata{$h}{"time"};
  my $data = $jsondata{$h}{"xml_smart"};
  next if ! $data;
  foreach my $d (split(/\<data/,$data)) {
    if ($d =~ /dev\=\"(.*)\"\>/) {
      my $dev = $1;
      #print "$h $dev\n";
      saveSmartData($time, $h, $dev, $d);
    }
  }
}

foreach my $h (sort keys %jsondata) {
  my $time = $jsondata{$h}{"time"};
  my $sysblock = $jsondata{$h}{"sys_block"};
  next unless $sysblock;
  foreach my $dev (sort keys %{$sysblock}) {
    next if $dev =~ m/^loop/;
    next if $dev =~ m/^ram/;
    my $smartctl = $jsondata{$h}{"sys_block"}{$dev}{"smartctl"};
    #print "[$h] [$dev] [$smartctl]\n";
    saveSmartData($time, $h, "/dev/".$dev, $smartctl);
  }
}

# figure out which machine is in which group

my %group_hostnames;

foreach my $h (sort keys %jsondata) {
  my $hostname = $jsondata{$h}{"hostname"};

  for my $g (sort keys %group_def) {
    my $def = $group_def{$g};
    my $x = $hostname =~ m/$def/;
    #print "[$hostname] [$g] [$def] [$x]\n";
    if ($x) {
      $jsondata{$h}{"groups"}{$g} = 1;
      $group_hostnames{$g}{$hostname} = 1;
    }
    $jsondata{$h}{"groups"}{"all"} = 1;
    $group_hostnames{"all"}{$hostname} = 1;
  }
}

my %alarms;
my %host_status;
my %host_alarms;

print "Generating main page\n" if $verbose;

my $out = "$confWeb/gonodereport.html";
open (OUT,">$out") || die "Cannot write to $out: $!";
print OUT genHtml("all");
close OUT;

for my $g (sort keys %group_def) {
  undef %alarms;
  undef %host_status;
  undef %host_alarms;

  print "Generating group [$g]\n" if $verbose;

  my $out = "$confWeb/group-$g.html";
  open (OUT,">$out") || die "Cannot write to $out: $!";
  print OUT genHtml($g);
  close OUT;
}

sendEmail();

exit 0;

sub saveSmartData
{
  my $time = shift @_;
  my $h = shift @_;
  my $dev = shift @_;
  my $d = shift @_;

  my $disk = "$h:$dev";

  #print "Save SMART data [$h] [$dev] [$disk] length ".length($d)."\n";

  return unless $d;
  return if $d =~ /Device Read Identity Failed/;
  return if $d =~ /Device does not support SMART/;
  return unless ($d =~ /Device Model/ || $d =~ /Model Number/);
  return unless $d =~ /Serial Number/;
      
  my $model;
  my $serno;
  
  if ($d =~ /^Device Model:\s+(.*)$/m) {
    $model = uc $1;
  }
  
  if ($d =~ /^Model Number:\s+(.*)$/m) {
    $model = uc $1;
  }
  
  if ($d =~ /^Serial Number:\s+(.*)$/m) {
    $serno = uc $1;
  }
  
  next unless $model;
  next unless $serno;
      
  #print "[$model] [$serno] [$disk] [$time]\n";
      
  my $f = "$model-$serno.txt";
  $f =~ s/\s+/-/g;
  $f =~ s/\//-/g;
  $f =~ s/\:/-/g;

  my $update = 1;
  my $fff = "$confWeb/disks/$f";
  
  if (-r $fff) {
    my $oldfile = readFile($fff);
    my ($olddisk, $oldtime) = split(/\n/, $oldfile);
    
    $oldtime = 0 unless $oldtime;
    $olddisk = "" unless $olddisk;
    
    $update = ($time <=> $oldtime);
  
    #print "[$f] $time $oldtime [$update] [$disk] [$olddisk]\n";
  }
  
  if ($update > 0) {
    writeFile("disks/$f", "$disk\n$time\n".$d);
  }
}

sub sendEmail
{
  my $haveAlarms = 0;
  my $emailtext;
  my $emailsubject;

  my $hostname = `hostname`;
  chop $hostname;

  $emailtext .= "Alarms from gonodereport on $hostname:\n";
  $emailtext .= "\n";

  foreach my $h (sort keys %email)
    {
      next if ! $email{$h};

      my $e = $email{$h};
      $e =~ s/\s+$//;
      #print "[$h] [$e]\n";

      $haveAlarms = 1;

      $emailsubject .= "$h($e) ";
      $emailtext .= "$h: $e\n";
    }

  return if !$haveAlarms;

  $emailsubject =~ s/\s+$//;

  $emailtext .= "\n--\ngonodereport.perl\n";

  my $cmd = "Mail -s \'gonodereport alarms: $emailsubject\' root";

  if ($verbose) {
    print "Email: $cmd\n";
    print $emailtext;
  }

  if ($email && !$noemail) {
    open(OUT, "|$cmd");
    print OUT $emailtext;
    close OUT;
  }
}

sub genHtml1
{
  my $group = shift @_;
  my $html;

  $html .= "<a name='gonodeinfo'></a>\n";

  $html .= genGonodeinfo($group);

  $html .= "<a name='nodeinfo'></a>\n";

  $html .= genNodeinfo($group);

  $html .= "<a name='misc'></a>\n";

  $html .= genMisc($group);

  $html .= "<a name='nisconfig'></a>\n";

  $html .= genNisconfig($group);

  $html .= "<a name='netconfig'></a>\n";

  $html .= genNetconfig($group);

  $html .= "<a name='storageconfig'></a>\n";

  $html .= genStorage($group);

  $html .= "<a name='dmidecode'></a>\n";

  $html .= genDmiconfig($group);

  $html .= "<a name='videoconfig'></a>\n";

  $html .= genVideoconfig($group);

  $html .= "<a name='pciconfig'></a>\n";

  $html .= genPciconfig($group);

  $html .= "<a name='ideconfig'></a>\n";

  $html .= genIdeconfig($group);

  $html .= "<a name='scsiconfig'></a>\n";

  $html .= genScsiconfig($group);

  $html .= "<a name='smartinfo'></a>\n";

  $html .= genSmartinfo($group);

  $html .= "<a name='diskscrub'></a>\n";

  $html .= genScrub($group);

  $html .= "<a name='zfs'></a>\n";

  $html .= genZfs($group);

  $html .= "<a name='mdstat'></a>\n";

  $html .= genMdstat($group);

  $html .= "<a name='md'></a>\n";

  $html .= genMd($group);

  $html .= "<a name='boot'></a>\n";

  $html .= genBoot($group);

  $html .= "<a name='yum'></a>\n";

  $html .= genYum($group);

  $html .= "<a name='packages'></a>\n";

  $html .= genRpm($group);

  $html .= "<a name='monitoring'></a>\n";

  $html .= genMonitoring($group);

  return $html;
}

sub genHtml
{
  my $group = shift @_;
  my $html1 = genHtml1($group);

  my $html = "";

  $html .= "<html>\n";
  $html .= "<title>Report for group $group</title>\n";
  $html .= "<h1>Nodeinfo report for group $group</h1>\n";

  $html .= "<p>Last updated: ".localtime(time())."</p>\n";

  $html .= "<p>Quick links:\n";
  $html .= "<a href='#main'>main</a>\n";
  $html .= "<a href='#alarms'>alarms</a>\n";
  $html .= "<a href='#gonodeinfo'>gonodeinfo</a>\n";
  $html .= "<a href='#nodeinfo'>nodeinfo</a>\n";
  $html .= "<a href='#misc'>misc</a>\n";
  $html .= "<a href='#nisconfig'>nis</a>\n";
  $html .= "<a href='#netconfig'>network</a>\n";
  $html .= "<a href='#storageconfig'>storage</a>\n";
  $html .= "<a href='#dmidecode'>dmidecode</a>\n";
  $html .= "<a href='#videoconfig'>video</a>\n";
  $html .= "<a href='#pciconfig'>pci</a>\n";
  $html .= "<a href='#ideconfig'>ide</a>\n";
  $html .= "<a href='#scsiconfig'>scsi</a>\n";
  $html .= "<a href='#smartinfo'>smartinfo</a>\n";
  $html .= "<a href='#diskscrub'>diskscrub</a>\n";
  $html .= "<a href='#zfs'>zfs</a>\n";
  $html .= "<a href='#mdstat'>mdstat</a>\n";
  $html .= "<a href='#md'>md raid</a>\n";
  $html .= "<a href='#boot'>boot</a>\n";
  $html .= "<a href='#yum'>yum</a>\n";
  $html .= "<a href='#packages'>packages</a>\n";
  $html .= "<a href='#monitoring'>monitoring</a>\n";
  #$html .= "<a href='#'></a>\n";
  $html .= "</p>\n";

  $html .= "<script type=\"text/javascript\" src=\"sorttable.js\"></script>\n";

  $html .= genCss($group);

  $html .= "<a name='main'></a>\n";

  $html .= genMain($group);

  $html .= "<a name='alarms'></a>\n";

  $html .= genAlarms($group);

  $html .= $html1;

  $html .= "</html>\n";

  return $html;
}

sub genCss
{
  my $group = shift @_;
  my $html;

  $html .= "<style type=\"text/css\">\n";
  foreach my $h (sort keys %class) {
    $html .= "tr.$class{$h}     { background-color: $classbg{$h} }\n" if defined $classbg{$h};
  
    if (defined $classbg{$h}) {
      $html .= "tr.$class{$h}main { background-color: $classbg{$h} }\n";
    } else {
      $html .= "tr.$class{$h}main { background-color: $classmain{$h} }\n" if defined $classmain{$h};
    }
  }
  $html .= "</style>\n";

  return $html;
}

sub genMain
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>Hardware and OS versions</h2>\n";

  $html .= "<table border=1 align=center class=\"sortable\">\n";

  $html .= "<thead>";
  $html .= "<tr>";
  $html .= "<th>Hostname</th>";
  #$html .= "<th align=center>!!!</th>";
  $html .= "<th align=center>Uptime</th>";
  $html .= "<th align=center>CPU</th>";
  $html .= "<th align=center>RAM</th>";
  $html .= "<th align=center>Swap</th>";
  $html .= "<th align=center>Packages</th>";
  $html .= "<th align=center>Updates</th>";
  $html .= "<th align=center>OS</th>";
  $html .= "<th align=center>Kernel</th>";
  $html .= "<th align=center>ZFS</th>";
  $html .= "<th align=center>Alarms</th>";
  $html .= "<th align=center>Description</th>";
  $html .= "</tr>\n";
  $html .= "</thead>";
  $html .= "<tbody>";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $hostname = $jsondata{$h}{"hostname"};
    #$hostname =~ s/\..+$// unless $hostname =~ /^\d+/;
    $hostname = $short_hostname{$hostname};

    my $uptime0  = $jsondata{$h}{"uptime"};
    my $cpuinfo  = $jsondata{$h}{"cpuinfo"};
    my $meminfo  = $jsondata{$h}{"meminfo"};
    my $linux    = $jsondata{$h}{"xml_linux"};
    if (!$linux) {
      $linux = $jsondata{$h}{"uname_r"};
    }

    my $zfs      = $jsondata{$h}{"zpool_version"};

    if ($zfs) {
      $zfs = "<0.8" if $zfs =~ /unrecognized command/;
      $zfs = "installed" if $zfs =~ /The ZFS modules are not loaded/;
      my ($zfsver, $kmodver) = $zfs =~ /zfs-(.*)\nzfs-kmod-(.*)/;
      if ($zfsver && $kmodver) {
        #print "$zfs: [$zfsver] [$kmodver]\n";
        $zfs = $zfsver if ($zfsver eq $kmodver);
      }
      $zfs =~ s/\s/<br>/;
    } else {
      $zfs = "";
    }

    #my $etcMD5   = findXmlTag($data,"etcMD5sum");

    #my $h = $hostname;

    my $uptime = "";
    if (defined $uptime0) {
      $uptime0 =~ /up (.*?),/;
      if ($1) {
	$uptime = $1;
	if ($uptime =~ /days/) {
	} else {
	  $uptime = "0 days";
	}
      }
    }

    my $redhat = $os{$hostname};

    #my $type = "";
    #if (defined $cpuinfo) {
    #  $type = "Intel" if $cpuinfo =~ /Intel/;
    #  $type = "AMD"   if $cpuinfo =~ /AMD/;
    #  $type = "ARM"   if $cpuinfo =~ /ARM/;
    #}

    my $cpu = "";
    my $cputype = "";

    if (defined $cpuinfo) {
      #my $count = 0;
      #my $mhz;
      #foreach my $line (split(/\n/,$cpuinfo)) {
      #  $mhz = $2 if ($line =~ /MHz(.*):(.*)/);
      #  $count ++ if ($line =~ /MHz(.*):(.*)/);
      #}
      #
      #if (defined $mhz) {
      #	$cpu = int($mhz);
      #	$cpu = $count."x".int($mhz) if ($count>1);
      #}

      ($cpu) = ($cpuinfo =~ m/model name\s+:\s+(.*)\n/);

      if ((!$cpu) || (length($cpu) < 1)) {
        ($cpu) = ($cpuinfo =~ m/Processor\s+:\s+(.*)\n/);
      }

      if ($cpu =~ m/Intel/ || $cpu =~ m/Pentium/) {
        $cputype = "Intel";
        $cpu =~ s/Intel\(R\) Xeon\(R\)//;
        $cpu =~ s/Intel\(R\) Xeon\(R\) CPU//;
        $cpu =~ s/Intel\(R\) Core\(TM\)//;
        $cpu =~ s/2 CPU//;
        $cpu =~ s/2 Duo CPU//;
        $cpu =~ s/2 Quad CPU//;
        $cpu =~ s/Mobile Intel\(R\) Pentium\(R\) 4/P-4/;
        $cpu =~ s/Intel\(R\) Pentium\(R\) III/P-III/;
        $cpu =~ s/Intel\(R\) Pentium\(R\) D/P-D/;
        $cpu =~ s/Genuine Intel\(R\) CPU//;
        $cpu =~ s/CPU//;
        $cpu =~ s/Intel Core Processor \(Haswell, no TSX, IBRS\)/VM/;
        $cpu =~ s/Pentium III \(Coppermine\)/P-III/;
      } elsif ($cpu =~ m/AMD/) {
        $cputype = "AMD";
        $cpu =~ s/AMD Athlon\(tm\)//;
        $cpu =~ s/with Radeon Graphics//;
        $cpu =~ s/with Radeon\(tm\) R3//;
        $cpu =~ s/AMD Ryzen Threadripper//;
        $cpu =~ s/AMD Ryzen 5//;
        $cpu =~ s/AMD Ryzen 7//;
        $cpu =~ s/6-Core Processor/6-Core/;
        $cpu =~ s/8-Core Processor/8-Core/;
        $cpu =~ s/12-Core Processor/12-Core/;
        $cpu =~ s/16-Core Processor/16-Core/;
        $cpu =~ s/^AMD.*E-450.*$/E-450/;
        $cpu =~ s/^AMD.*E-350.*$/E-350/;
        #print "AMD CPU [$cpu]\n";
      } elsif ($cpu =~ m/ARM/) {
        $cputype = "ARM";
        ($cpu) = ($cpuinfo =~ m/Hardware\s+:\s+(.*)\n/);
      } else {
        $cputype = $cpu;
      }

      $cpu =~ s/^\s+//; # kill leading spaces
      $cpu =~ s/\s+$//; # kill trailing spaces
      $cpu =~ s/\s+/ /; # replace multiple spaces
      #$cpu =~ s/\s+/&nbsp;/g; # replace spaces with &nbsp;
    }

    my $totalram  = "";
    my $totalswap = "";
    if (defined $meminfo) {
      if ($meminfo =~ /Mem:\s+(\d+)/) {
        $totalram = int(int($1)/(1024*1024));
      }

      if ($meminfo =~ /MemTotal:\s+(\d+)/) {
        $totalram = int(int($1)/(1024));
      }

      if ($meminfo =~ /Swap:\s+(\d+)/) {
        $totalswap = int(int($1)/(1024*1024));
      }

      if ($meminfo =~ /SwapTotal:\s+(\d+)/) {
        $totalswap = int(int($1)/(1024));
      }
    }

    my $yum = "";
    my $yum_updated = $jsondata{$h}{"xml_yum_updated"};

    if (!defined $yum_updated) {
      $yum = "(unknown)";
    } else {
      if (defined $yum_updated && length $yum_updated > 0) {
        #print "$hostname: $yum_updated\n";
        if ($yum_updated =~ /^(\/.*?)\:(.*)/) {
          $yum_updated = $2;
          #print "[$1][$2] $yum_updated\n";
        }
        my ($mon, $day) = split(/\s+/, $yum_updated);
        $yum .= " $mon $day";
        #print " yum [$yum]\n";
      }

      if (!yum_lock_ok($hostname)) {
        $yum .= "<br>" unless length $yum < 2;
        $yum .= "no<br>nightly<br>updates";
      }
    }

    if ($stale{$hostname}) {
      my $time = $jsondata{$h}{"time"};
      $uptime = -int((time() - $time)/(24*60*60));
    }

    my $a = $alarms{$h};
    $a = "" if ! defined $a;

    $html .= "<tr class=$class{$h}main>";
    $html .= "<td align=center>$hostname</td>";
    #$html .= "<td align=center id=$class{$h}alarm><a href='#alarms_$h'>*</a></td>";
    $html .= "<td align=right style=\"white-space: nowrap\">$uptime</td>";
    $html .= "<td align=left style=\"white-space: nowrap\">$cputype $cpu</td>";
    $html .= "<td align=right>$totalram</td>";
    $html .= "<td align=right>$totalswap</td>";
    if ($jsondata{$h}{"split_rpms"}) {
      $html .= htmltag("td","align=right",0+@{$jsondata{$h}{"split_rpms"}});
    } else {
      $html .= htmltag("td","align=right","???");
    }
    $html .= htmltag("td","align=center",$yum);
    $html .= "<td style=\"white-space: nowrap\">$redhat</td>";
    $html .= "<td style=\"white-space: nowrap\">$linux</td>";
    $html .= "<td style=\"white-space: nowrap\">$zfs</td>";
    $html .= "<td>$a</td>";
    $html .= "<td align=left>$description{$hostname}</td>";

    $html .= "</tr>\n";
  }

  $html .= "</tbody>";
  $html .= "</table>\n";

  return $html;
}

sub genGonodeinfo
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>gonodeinfo.conf</h2>\n";

  my %headers;
  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $gonodeinfo = $jsondata{$h}{"config"};
    foreach my $k (keys %{$gonodeinfo}) {
      my $v = $jsondata{$h}{"config"}{$k};
      #print "[$h] [$k] [$v]\n";
      $headers{$k} += 1;
    }
  }

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<thead>\n";
  $html .= "<th>Hostname</th>\n";
  $html .= "<th>Description</th>\n";
  foreach my $k (sort keys %headers) {
    next if $k eq "Description";
    $html .= "<th>" . $k . "</th>\n";
  }
  $html .= "</thead>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    #my $gonodeinfo = $jsondata{$h}{"config"};
    #next unless $gonodeinfo;

    my $description = $jsondata{$h}{"config"}{"Description"};
    next unless $description;
    #next unless length($description) > 1;

    $html .= "<tr class=$class{$h}>";
    $html .= htmltag("td", "align=center", $h);
    $html .= htmltag("td", "align=left", $description);
    foreach my $k (sort keys %headers) {
      next if $k eq "Description";
      my $v = $jsondata{$h}{"config"}{$k};
      #print "[$h] [$k] [$v]\n";
      $html .= htmltag("td", "align=left", $v);
    }
    $html .= "</tr>\n";
  }

  $html .= "</table>\n";

  return $html;
}

sub genNodeinfo
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>Node information</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","Description"));
  foreach my $hdr (@descriptionheaders) {
    $html .= htmltag("td","align=center",htmltag("b","","$hdr"));
  }
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $etcnodeinfo = $jsondata{$h}{"xml_etcnodeinfo"};
    next unless $etcnodeinfo;

    $html .= "<tr class=$class{$h}>";
    $html .= htmltag("td","align=center",$h);
    $html .= htmltag("td","align=left",$description{$h});
    foreach my $hdr (@descriptionheaders) {
      my $value = "";
      if (defined $etcnodeinfo) {
        if ($etcnodeinfo =~ /^$hdr\s*:\s*(.*?)$/m) {
          $value = $1;
        }
      }
      $html .= htmltag("td","align=left",$value);
    }
    $html .= "</tr>\n";
  }

  $html .= "</table>\n";

  return $html;
}

sub genMisc
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>Misc configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","Printers"));
  $html .= htmltag("td","align=center",htmltag("b","","Time server"));
  $html .= htmltag("td","align=center",htmltag("b","","root .forward"));
  $html .= htmltag("td","align=center",htmltag("b","","tcp wrappers"));
  $html .= htmltag("td","align=center",htmltag("b","","Mail spool"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    $html .= "<tr class=$class{$h}>";
    $html .= htmltag("td","align=center",$h);

    if (my $printcap = $jsondata{$h}{"xml_printcap"}) {
      if ($printcap =~ /cupsd/) {
        $html .= htmltag("td","align=center","cups");
      } else {
        my @lp = split(/\n/,$printcap);
        my $out = "";
        foreach my $printer (@lp) {
          next if $printer =~ /^#/;
          $printer =~ s/:.*$//;
          $out .= " ".$printer;
        }
        $html .= htmltag("td","",$out);
      }
    } else {
      $html .= htmltag("td", "align=center", "???");
      #setStatus($h, 2, "CUPS");
      #$html .= htmltag("td","bgcolor=red align=center","none");
      #setStatus($h, 2, "CUPS");
    }

    if (my $stat = $jsondata{$h}{"chronyc_tracking"}) {
      if (lc($stat) =~ /reference id.*\((.*)\)/) {
        $html .= htmltag("td","align=center",$1);
      } else {
        $html .= htmltag("td","bgcolor=red align=center","none");
        setStatus($h, 2, "NTP", 1);
      }
    } elsif (my $ntp = $jsondata{$h}{"ntptrace"}) {
      #$html .= htmltag("td","","$ntp");
      my $x = "";
      foreach my $d (split(/\n/,$ntp)) {
        next if $d =~ /^local/;
        $d =~ /^(.*):/;
        my $host = $1;
        $host =~ s/\.(.*)$//;
        $x .= "$host ";
        last;
      }
      $html .= htmltag("td","align=center","$x");
    } elsif (my $ntpstat = $jsondata{$h}{"ntpstat"}) {
      if ($ntpstat =~ /synchronised to NTP server \((.*)\)/) {
        $html .= htmltag("td","align=center",$1);
      } else {
        $html .= htmltag("td","bgcolor=red align=center","none");
        setStatus($h, 2, "NTP", 1);
      }
    } elsif (my $tts = $jsondata{$h}{"timedatectl_timesync_status"}) {
      my ($server) = $tts =~ /Server:\s+(.*)\n/;
      my ($stratum) = $tts =~ /Stratum:\s+(.*)\n/;
      #print "Server [$server] [$stratum]\n";
      if ($stratum) {
        $html .= htmltag("td","align=center",$server);
      } else {
        $html .= htmltag("td","bgcolor=red align=center",$server);
        setStatus($h, 2, "NTP", 1);
      }
    } else {
      $html .= htmltag("td","bgcolor=red align=center","none");
      setStatus($h, 2, "NTP", 1);
    }

    if (my $ret = $jsondata{$h}{"root_dot_forward"}) {
      $html .= htmltag("td","align=center","yes");
    } else {
      $html .= htmltag("td","align=center","?");
    }

    my $tcp_wrappers = "";
    if (my $ret = $jsondata{$h}{"hosts_allow"}) {
      if (length($ret) < 1) {
        $tcp_wrappers .= "zero hosts_allow<br>";
      } else {
        my @allow = grep(!/^#/, split(/\n/, $ret));
        if (@allow < 1) {
          #$tcp_wrappers .= "empty hosts_allow<br>";
        } else {
          $tcp_wrappers .= "yes hosts_allow<br>";
          #$tcp_wrappers .= join("XXX", @allow);
        }
      }
    } else {
      $tcp_wrappers .= "???<br>";
    }

    if (my $ret = $jsondata{$h}{"hosts_deny"}) {
      if (length($ret) < 1) {
        $tcp_wrappers .= "zero hosts_deny<br>";
      } else {
        my @allow = grep(!/^#/, split(/\n/, $ret));
        if (@allow < 1) {
          #$tcp_wrappers .= "empty hosts_deny<br>";
        } else {
          $tcp_wrappers .= "yes hosts_deny<br>";
          #$tcp_wrappers .= join("XXX", @allow);
        }
      }
    } else {
      $tcp_wrappers .= "???<br>";
    }

    $html .= htmltag("td", "align=center align=center style=\"white-space: nowrap\"", $tcp_wrappers);

    if (my $mail = $jsondata{$h}{"var_spool_mail"}) {
      #$html .= htmltag("td","","$mail");
      my $x = "";
      foreach my $d (split(/\n/,$mail)) {
        next if $d =~ /^total/;
        my @d = split(/ +/,$d);
        my $name = pop @d;
        my $size = $d[4];
        $x .= "$name($size) ";
      }
      $html .= htmltag("td","","$x");
    } else {
      $html .= htmltag("td","bgcolor=red align=center","none");
      setStatus($h, 2, "MAIL");
    }

    $html .= "</tr>\n";
  }

  $html .= "</table>\n";
  
  return $html;
}

sub genDmiconfig
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>DMI information</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","Motherboard"));
  $html .= htmltag("td","align=center",htmltag("b","","BIOS"));
  #$html .= htmltag("td","align=center",htmltag("b","","Socket"));
  $html .= htmltag("td","align=center",htmltag("b","","RAM"));
  $html .= "</tr>\n";
  
  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $dmi = $jsondata{$h}{"dmidecode"};

    if (defined $dmi) {
      $dmi =~ /BIOS Information.*?Version:(.*?)Release/s;
      my $biosver = $1;
      $biosver = "" unless $biosver;
      $biosver =~ s/^\s+//;
      $biosver =~ s/\s+$//;

      #print "$h [$biosver]\n";

      $dmi =~ /Base Board Information.*?Manufacturer:(.*?)Product/s;
      my $mobomanu = $1;
      $mobomanu = "" unless $mobomanu;
      $mobomanu = "" if $mobomanu =~ /To be filled/;
      $mobomanu =~ s/^\s+//;
      $mobomanu =~ s/\s+$//;

      $mobomanu = "ASUS"     if $mobomanu =~ /ASUS/;
      $mobomanu = "Intel"    if $mobomanu =~ /intel/i;
      $mobomanu = "Gigabyte" if $mobomanu =~ /gigabyte/i;
      $mobomanu = "Dell"     if $mobomanu =~ /dell/i;

      $dmi =~ /Base Board Information.*?Product Name:(.*?)Version/s;
      my $mobo = $1;
      $mobo = "" unless $mobo;
      $mobo = "" if $mobo =~ /To be filled/;
      $mobo =~ s/^\s+//;
      $mobo =~ s/\s+$//;

      $mobo =~ s/&lt;//;
      $mobo =~ s/&gt;//;

      #$mobo = "ASUS ".$mobo if $biosver =~ /ASUS/;

      $dmi =~ /Base Board Information.*?Version:(.*?)Serial/s;
      my $mobover = $1;
      $mobover = "" unless $mobover;
      $mobover = "" if $mobover =~ /To be filled/;
      $mobover =~ s/^\s+//;
      $mobover =~ s/\s+$//;

      #my $conf = "$mobomanu $mobo $mobover";
      my $conf = "$mobomanu $mobo";

      if ($mobomanu eq "Intel") {
        $biosver = "???";
      }

      my $ram = "";

      foreach my $s (split(/Handle 0x/, $dmi)) {
        #print "DMI $h [$s]\n";
        if ($s =~ m/Memory Device Mapped Address/) {
        } elsif ($s =~ m/Processor/) {
#       Processor Information
#       Socket Designation: LGA1151
#       Type: Central Processor
# 	Family: Core i5
# 	Manufacturer: Intel(R) Corporation
# 	ID: E9 06 09 00 FF FB EB BF
# 	Signature: Type 0, Family 6, Model 158, Stepping 9
# 	Flags:
# 		FPU (Floating-point unit on-chip)
# 		VME (Virtual mode extension)
# 		DE (Debugging extension)
# 		PSE (Page size extension)
# 		TSC (Time stamp counter)
# 		MSR (Model specific registers)
# 		PAE (Physical address extension)
# 		MCE (Machine check exception)
# 		CX8 (CMPXCHG8 instruction supported)
# 		APIC (On-chip APIC hardware supported)
# 		SEP (Fast system call)
# 		MTRR (Memory type range registers)
# 		PGE (Page global enable)
# 		MCA (Machine check architecture)
# 		CMOV (Conditional move instruction supported)
# 		PAT (Page attribute table)
# 		PSE-36 (36-bit page size extension)
# 		CLFSH (CLFLUSH instruction supported)
# 		DS (Debug store)
# 		ACPI (ACPI supported)
# 		MMX (MMX technology supported)
# 		FXSR (FXSAVE and FXSTOR instructions supported)
# 		SSE (Streaming SIMD extensions)
# 		SSE2 (Streaming SIMD extensions 2)
# 		SS (Self-snoop)
# 		HTT (Multi-threading)
# 		TM (Thermal monitor supported)
# 		PBE (Pending break enabled)
# 	Version: Intel(R) Core(TM) i5-7600K CPU @ 3.80GHz
# 	Voltage: 1.0 V
# 	External Clock: 100 MHz
# 	Max Speed: 8300 MHz
# 	Current Speed: 3800 MHz
# 	Status: Populated, Enabled
# 	Upgrade: Other
# 	L1 Cache Handle: 0x0049
# 	L2 Cache Handle: 0x004A
# 	L3 Cache Handle: 0x004B
# 	Serial Number: To Be Filled By O.E.M.
# 	Asset Tag: To Be Filled By O.E.M.
# 	Part Number: To Be Filled By O.E.M.
# 	Core Count: 4
# 	Core Enabled: 4
# 	Thread Count: 4
# 	Characteristics:
# 		64-bit capable
# 		Multi-Core
# 		Execute Protection
# 		Enhanced Virtualization
# 		Power/Performance Control

          #my ($xsocket)  = ($s =~ m/Socket Designation:\s+(.*)\n/);
          #$socket = $xsocket;
        } elsif ($s =~ m/Memory Device/) {
          #// parse:
          #// Memory Device
          #//       Array Handle: 0x0027
          #//       Error Information Handle: No Error
          #//       Total Width: 72 bits
          #//       Data Width: 64 bits
          #//       Size: 4096 MB
          #//       Form Factor: DIMM
          #//       Set: None
          #//       Locator: DIMM_2A
          #//       Bank Locator: BANK1
          #//       Type: DDR3
          #//       Type Detail: Synchronous
          #//       Speed: 1333 MHz
          #//       Manufacturer: Kingston
          #//       Serial Number: 32237C0D
          #//       Asset Tag: A1_AssetTagNum1
          #//       Part Number: 9965525-018.A00LF
          #//       Rank: 2
          #//       Configured Clock Speed: 32768 MHz
          #
          # daq00:
          #
          # Memory Device
          # Array Handle: 0x0024
          # Error Information Handle: Not Provided
          # Total Width: 72 bits
          # Data Width: 64 bits
          # Size: 16384 MB
          # Form Factor: DIMM
          # Set: None
          # Locator: DIMMA1
          # Bank Locator: P0_Node0_Channel0_Dimm0
          # Type: DDR4
          # Type Detail: Synchronous
          # Speed: 3200 MT/s
          # Manufacturer: Undefined
          # Serial Number: 2B9500D5
          # Asset Tag: 9876543210
          # Part Number:                     
          # Rank: 2
          # Configured Memory Speed: 2667 MT/s
          # Minimum Voltage: 1.2 V
          # Maximum Voltage: 1.2 V
          # Configured Voltage: 1.2 V
          # Memory Technology: DRAM
          # Memory Operating Mode Capability: Volatile memory
          # Firmware Version: Not Specified
          # Module Manufacturer ID: Unknown
          # Module Product ID: Unknown
          # Memory Subsystem Controller Manufacturer ID: Unknown
          # Memory Subsystem Controller Product ID: Unknown
          # Non-Volatile Size: None
          # Volatile Size: 16 GB
          # Cache Size: None
          # Logical Size: None
          #
          my ($loc)  = ($s =~ m/Locator:\s+(.*)\n/);
          my ($size) = ($s =~ m/Size:\s+(.*)\n/);
          my ($type) = ($s =~ m/Type:\s+(.*)\n/);
          my ($speed) = ($s =~ m/Speed:\s+(.*)\n/);
          my ($conf_speed) = ($s =~ m/Configured Clock Speed:\s+(.*)\n/);
          ($conf_speed) = ($s =~ m/Configured Memory Speed:\s+(.*)\n/) unless $conf_speed;
          my ($voltage) = ($s =~ m/Configured Voltage:\s+(.*)\n/);
          my ($width) = ($s =~ m/Data Width:\s+(.*)\n/);
          my ($total_width) = ($s =~ m/Total Width:\s+(.*)\n/);
          my $xecc = "";
          $xecc = "ECC" if ($width ne $total_width);

          $conf_speed = $speed if !$conf_speed;
          $voltage = "" if !$voltage;

          if ($size =~ m/No Module/) {
            $ram .= "$loc $size $voltage";
          } elsif ($speed eq $conf_speed) {
            $ram .= "$loc $size $xecc $type $speed $voltage";
          } else {
            $ram .= "$loc $size $xecc $type $speed actual $conf_speed $voltage";
          }
          $ram .= "<br>";
        }
      }

      #print "RAM [$h] [$ram]\n";

      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=center",$h);
      $html .= htmltag("td","align=left",$conf);
      $html .= htmltag("td","align=left",$biosver);
      #$html .= htmltag("td","align=left",$socket);
      $html .= htmltag("td","align=left",$ram);
      $html .= "</tr>\n";

      #print "RAM $ram\n";
      #die "Here" if $h =~ m/alpha02/;
    }
  }
  
  $html .= "</table>\n";

  return $html;
}

sub genVideoconfig
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>Video cards</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","Vendor"));
  $html .= htmltag("td","align=center",htmltag("b","","Model"));
  $html .= htmltag("td","align=center",htmltag("b","","lspci"));
  $html .= "</tr>\n";
  
  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $conf = $jsondata{$h}{"lspci"};
      
    if (defined $conf) {
      my @conf = split(/\n/, $conf);

      foreach my $pci (sort @conf) {
        #$pci =~ s/Advanced Micro Devices \[AMD\] nee //;
        #$pci =~ s/Advanced Micro Devices, Inc./amdxati/;

        next unless $pci =~ /VGA.*: (\w+)/;
        my $v = lc($1);

        my $m = "";
        $m = $1 if ($pci =~ /\[(.*)\]/);

        if ($pci =~ /Intel/) {

          if (!$m) {
            $m = "Intel $1" if ($pci =~ /Intel Corporation Mobile (\w+)/);
          }
          if (!$m) {
            $m = "Intel $1" if ($pci =~ /Intel Corporation (\w+).*Chipset/);
          }
          if (!$m) {
            $m = "Intel $1" if ($pci =~ /Intel Corporation (\w+).*Integrated/);
          }
        }

        if ((lc $pci) =~ /nvidia corporation\s+(.*)/) {
          $v = "nvidia";
          $m = "nvidia " . $1;
          $m =~ s/\(rev.*\)//;
        }

        if ((lc $pci) =~ /advanced micro devices.* nee ati\s+(.*)/) {
          $v = "amd/ati";
          $m = "amd " . $1;
          $m =~ s/\(rev.*\)//;
        }

        if ((lc $pci) =~ /\[amd\/ati\]\s+(.*)/) {
          $v = "amd/ati";
          $m = "amd " . $1;
          $m =~ s/\(rev.*\)//;
        }

        $pci =~ s/VGA compatible controller//;

        $m =~ s/\s+$//;

        #print "$h [$m]\n" if $m =~ /es1000/;

        $html .= "<tr class=$class{$h}>";
        $html .= htmltag("td","align=center",$h);
        $html .= htmltag("td","align=left",$v);
        $html .= htmltag("td","align=left",$m);
        $html .= htmltag("td","align=left",$pci);
        $html .= "</tr>\n";
      }
    }
  }
  
  $html .= "</table>\n";

  return $html;
}

sub genPciconfig
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>PCI configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","lspci"));
  $html .= "</tr>\n";
  
  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $conf = $jsondata{$h}{"lspci"};
      
    if (defined $conf) {
      $conf =~ s/\n/\<br\>/mg;
      $conf =~ s/ /\&nbsp\;/mg;
      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=center",$h);
      $html .= htmltag("td","align=left",$conf);
      $html .= "</tr>\n";
    }
  }
  
  $html .= "</table>\n";

  return $html;
}

sub genIdeconfig
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>IDE configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","Slot"));
  $html .= htmltag("td","align=center",htmltag("b","","Capacity"));
  $html .= htmltag("td","align=center",htmltag("b","","Cache"));
  $html .= htmltag("td","align=left",htmltag("b","","Identity"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $ideconf = $jsondata{$h}{"xml_ide"};

    if (defined $ideconf) {
      my ($slots, $caps, $caches, $models);
      foreach my $dev (sort split(/\n/,$ideconf)) {
        my ($slot,$cap,$cache,$model) = split(/\:/,$dev);
        my $capGB = int($cap/(2*1000*1000));
        # some CDROMs report non-zero capacity
        if (($capGB == 1073)||($capGB == 0)) {
          $capGB = "(removable)";
        } else {
          $capGB .= " GBytes" if ($capGB > 0);
        }
        $cache = "" if ($cache eq "none");

        $slots .= "$slot<br>";
        $caps  .= "$capGB<br>";
        $caches.= "$cache<br>";
        $models.= "$model<br>";
      }

      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=center",$h);
      if (defined $slots) {
        $html .= htmltag("td","align=center",$slots);
        $html .= htmltag("td","align=right",$caps);
        $html .= htmltag("td","align=right",$caches);
        $html .= htmltag("td","align=left",$models);
      } else {
        $html .= htmltag("td","","");
        $html .= htmltag("td","","");
        $html .= htmltag("td","","");
        $html .= htmltag("td","","");
      }
      $html .= "</tr>\n";
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub genScsiconfig
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>SCSI configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","/proc/scsi/scsi"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $conf = $jsondata{$h}{"scsi"};

    if (defined $conf) {
      $conf =~ s/\n/\<br\>/mg;
      $conf =~ s/ /\&nbsp\;/mg;
      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=center",$h);
      $html .= htmltag("td","align=left",$conf);
      $html .= "</tr>\n";
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub genSmartinfo
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>SMART disk diagnostics</h2>\n";

  my $now = time();
  my %smart;
  my $count = 1;

  my @files = split(/\n/, `/bin/ls -1 $confWeb/disks`);

  foreach my $f (sort @files) {
    next unless $f =~ /.txt$/;
    #print "$f\n";

    my $dd = readFile($confWeb."/disks/".$f);

    my ($diskmnt, $time, $d) = ($dd =~ /(.*?)\n(.*?)\n(.*)$/s);

    #my $disk = $diskmnt;
    my $disk = $diskmnt.":".$count++;

    my $age = $now - $time;
    my $age_days = int($age/24/60/60);
    $age_days = 1 if $age_days < 1;

    #print "$f $diskmnt $time $age $age_days\n";

    #print "$f $diskmnt $time $d\n";

    my ($h, $foo) = split(/\:/, $diskmnt);
    if ($group ne "all") {
      next unless $jsondata{$h}{"groups"}{$group};
    }

    next if $d =~ /Device Read Identity Failed/;
      
    my $disktype = "unknown";
      
    if ($d =~ /Device: (\w+)\W(.*)\s+Supports/) {
      $disktype  = uc($1);
      my $diskmodel = $2;
          
      if (lc($disktype) =~ /^st/) {
        $diskmodel = $disktype;
        $disktype = "Seagate";
      }
          
      if (lc($disktype) =~ /^ic/) {
        $diskmodel = $disktype;
        $disktype = "IBM";
      }
          
      $smart{$disktype}{"attr"}{"00model"} = "model";
      $smart{$disktype}{$disk}{"00model"} = $diskmodel;
          
      #print "A [$disk] [$disktype] [$diskmodel]\n";
    } elsif ($d =~ /^Device Model:\s+(\w+)\s+(.*)$/m) {
      $disktype  = uc($1);
      my $diskmodel = $2;
          
      if (lc($disktype) =~ /^st/) {
        $diskmodel = $disktype;
        $disktype = "Seagate";
      }
          
      if (lc($disktype) =~ /^ic/) {
        $diskmodel = $disktype;
        $disktype = "IBM";
      }

      #if (uc($diskmodel) =~ /WD\d\d\d\d/) {
      #  $disktype = "WDC-A-100-999-GB";
      #} elsif (uc($diskmodel) =~ /WD\d\d\d/) {
      #  $disktype = "WDC-B-1-99-GB";
      #}

      if (($disktype eq "WDC") && ($diskmodel =~ /WDS\d+/)) {
        $disktype = "WDC-SSD-WDS";
      }
          
      if (($disktype eq "WD") && ($diskmodel =~ /SA510/)) {
        $disktype = "WDC-SSD-SA510";
      }

      if (($disktype eq "WDC") && !($d =~ /User Capacity:.*TB/)) {
        $disktype = "WDC-small";
      }
          
      #if (($disktype eq "KINGSTON") && ($diskmodel =~ /SVP\d+/)) {
      #  $disktype = "KINGSTON-SVP";
      #}
          
      if (($disktype eq "KINGSTON") && ($diskmodel =~ /SV300/)) {
        $disktype = "KINGSTON-SV300";
      }
          
      if (($disktype eq "KINGSTON") && ($diskmodel =~ /SM\d+/)) {
        $disktype = "KINGSTON-SM";
      }
          
      if (($disktype eq "KINGSTON") && ($diskmodel =~ /SA\d+/)) {
        $disktype = "KINGSTON-SA";
      }
          
      $smart{$disktype}{"attr"}{"00model"} = "model";
      $smart{$disktype}{$disk}{"00model"} = $diskmodel;
          
      #print "B [$disk] [$disktype] [$diskmodel]\n";
    } elsif ($d =~ /^Device Model:\s+(\w+)\W+(.*)$/m) {
      $disktype  = uc($1);
      my $diskmodel = $2;
          
      if (lc($disktype) =~ /^st/) {
        $diskmodel = $disktype;
        $disktype = "Seagate";
      }
          
      if (lc($disktype) =~ /^ic/) {
        $diskmodel = $disktype;
        $disktype = "IBM";
      }
          
      $smart{$disktype}{"attr"}{"00model"} = "model";
      $smart{$disktype}{$disk}{"00model"} = $diskmodel;
          
      #print "C [$disk] [$disktype] [$diskmodel]\n";
    }
      
    if ($d =~ /^Serial Number:\s+(.*)$/m) {
      my $serno = $1;
      my $sernolink = "<a href=\"disks/$f\">$serno</a>";
      $smart{$disktype}{"attr"}{"00serno"} = "serno";
      $smart{$disktype}{$disk}{"00serno"} = $sernolink;
    }
      
    if ($d =~ /^Firmware Version:\s+(.*)$/m) {
      $smart{$disktype}{"attr"}{"01firmware"} = "firmware";
      $smart{$disktype}{$disk}{"01firmware"} = $1;
    }

    $smart{$disktype}{"attr"}{"00age"} = "Age, days";
    $smart{$disktype}{$disk}{"00age"} = $age_days;

    $smart{$disktype}{"attr"}{"00aaamnt"} = "Mount point";
    $smart{$disktype}{$disk}{"00aaamnt"} = $diskmnt;

    foreach my $a (split(/\n/,$d)) {
      if ($a =~ /\((..\d)\)(.+?)\s+0x\w+\s+\d+\s+\d+\s+\d+\s+(\d+)/) {
        # parse RHL8.0-9 smartctl data
              
        my $attr = int($1);
        my $name = $2;
        my $val  = int($3);
              
        next if $attr <= 0;
        next if $val == 0;
              
        #print "$disk $attr $name $val\n";
              
        $smart{$disktype}{"attr"}{$attr} = $name;
        $smart{$disktype}{$disk}{$attr} = $val;
      } elsif ($a =~ /ATA Error Count:\s+(\d+)/) {
        # parse RHL8.0-9 smartctl data
              
        my $attr = "02ataerrors";
        my $name = "ATA Error Count";
        my $val  = int($1);
              
        next if $val == 0;
              
        #print "$disk $attr $name $val\n";
              
        $smart{$disktype}{"attr"}{$attr} = $name;
        $smart{$disktype}{$disk}{$attr} = $val;
      } elsif ($a =~ /(..\d)\s(\S+)\s+0x\w+\s+\d+\s+\d+\s+\d+\s+\S+\s+\S+\s+(\d+)/) {
        # parse Fedora-1 smartctl version 5.1-11 data

              
        my $attr = int($1);
        my $name = $2;
        my $val  = int($3);
              
        next if $attr <= 0;
        next if $val == 0;
              
        #print "$disk $attr $name $val\n";
              
        $smart{$disktype}{"attr"}{$attr} = $name;
        $smart{$disktype}{$disk}{$attr} = $val;
      } elsif ($a =~ /(..\d)\s(\S+)\s+0x\w+\s+\d+\s+\d+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\d+)/) {
        # parse smartctl version 5.26 data
	# 232 Available_Reservd_Space 0x0033   100   100   004    Pre-fail  Always       -       100
	# 233 Media_Wearout_Indicator 0x0032   100   100   ---    Old_age   Always       -       10540

              
        my $attr = int($1);
        my $name = $2;
        my $val  = int($3);
              
        next if $attr <= 0;
        next if $val == 0;
              
        #print "$disk $attr $name $val\n";
              
        $smart{$disktype}{"attr"}{$attr} = $name;
        $smart{$disktype}{$disk}{$attr} = $val;
      }
    }
  }

  foreach my $disktype (sort keys %smart) {
    #delete $smart{$disktype}{"attr"}{"00model"};
    #delete $smart{$disktype}{"attr"}{"00serno"};
    #delete $smart{$disktype}{"attr"}{"01firmware"};
    if ($disktype ne "WDC") {
      #print "delete $disktype\n";
      delete $smart{$disktype}{"attr"}{1}; # "raw read error rate"
    }
    delete $smart{$disktype}{"attr"}{3}; # "spin up time"
    delete $smart{$disktype}{"attr"}{7}; # "seek error rate"
    delete $smart{$disktype}{"attr"}{8}; # seek time performance

    #delete $smart{$disktype}{"attr"}{187}; # seagate 187 Reported_Uncorrect
    delete $smart{$disktype}{"attr"}{188}; # seagate 188 Command_Timeout
    delete $smart{$disktype}{"attr"}{189}; # seagate 189 High_Fly_Writes
    delete $smart{$disktype}{"attr"}{190}; # seagate 190 Airflow_Temperature_Cel
    # seagate 191 G-Sense_Error_Rate      0x0032   100   100   000    Old_age   Always       -       0
    # seagate 192 Power-Off_Retract_Count 0x0032   100   100   000    Old_age   Always       -       63
    # seagate 193 Load_Cycle_Count        0x0032   098   098   000    Old_age   Always       -       5259
    # seagate 194 Temperature_Celsius     0x0022   027   050   000    Old_age   Always       -       27 (0 13 0 0 0)
    delete $smart{$disktype}{"attr"}{195}; # "hardware ecc recovered"
    #delete $smart{$disktype}{"attr"}{197}; # current pending sector
    delete $smart{$disktype}{"attr"}{209}; # offline seek time performance
    delete $smart{$disktype}{"attr"}{201}; # "soft read error rate
    delete $smart{$disktype}{"attr"}{240}; # Seagate Head_Flying_Hours
    delete $smart{$disktype}{"attr"}{241}; # Seagate Total_LBAs_Written
    delete $smart{$disktype}{"attr"}{242}; # Seagate Total_LBAs_Read
      
    $html .= "<h3>SMART disk diagnostics for $disktype disks</h3>\n";

    $html .= "<table border=1 align=\"center\" class=\"sortable\">\n";

    $html .= "<tr>";
    $html .= htmltag("td","align=center",htmltag("b","","Disk"));

    my @attr = sort keys %{$smart{$disktype}{"attr"}};
    foreach my $attr (@attr) {
      next if $attr eq "00aaamnt";
      my $attrname = $smart{$disktype}{"attr"}{$attr};
      $attrname = $attr if ! $attrname;
      $attrname = lc($attrname);
      $attrname =~ s/\_/ /g;
      $attrname = $attr if ($attrname eq "unknown attribute");
      $attrname = "multi zone error rate" if ($disktype eq "WDC") && ($attrname eq "200");
      $attrname = "soft read error rate" if ($attrname eq "201");
      $attrname = "ECC errors" if ($disktype eq "MAXTOR") && ($attrname eq "203");
      $attrname =~ s/temperature/temp<br>era<br>ture/;
      $attrname =~ s/reallocated/re<br>allo<br>cated/;
      $attrname =~ s/power-off/power<br>off/;
      $attrname =~ s/uncorrectable/un<br>correct<br>able/;
      $attrname =~ s/performnce/performance/;
      $attrname =~ s/preformance/per<br>for<br>mance/;
      $attrname =~ s/performance/per<br>for<br>mance/;
      $attrname =~ s/hardware/hard<br>ware/;
      $attrname =~ s/recovered/reco<br>vered/;
      $attrname =~ s/calibration/calib<br>ration/;
      $attrname =~ s/throughput/thru<br>put/;
      $attrname =~ s/firmware/fw/;
      #$attrname = $attr."<br>".$attrname;
      $html .= htmltag("td","align=center",htmltag("b","",$attrname));
    }
    $html .= "</tr>\n";

    foreach my $disk (sort keys %{$smart{$disktype}}) {
      next if $disk eq "attr";

      my $diskmnt = $smart{$disktype}{$disk}{"00aaamnt"};

      my ($h, $foo) = split(/\:/, $diskmnt);

      my $line = "";
      my $diskcode = 0;

      my $age_days = $smart{$disktype}{$disk}{"00age"};
      my $is_old = ($age_days > 2);

      foreach my $attr (@attr) {
        next if $attr eq "00aaamnt";

        my $attrval = $smart{$disktype}{$disk}{$attr};
        $attrval = "" if ! $attrval;

        my $xattrval = $attrval;
        $attrval =~ s/\W/ /g;

        my $code = 0;

        if ($attrval) {
	  if ($disktype eq "WDC") {
	    $code |= 1 if (($attr eq "1")&&($attrval >= 1000)); # Raw_Read_Error_Rate
	  }
          $code |= 1 if (($attr eq "196")&&($attrval >= 100)); # reallocated event count
          $code |= 1 if (($attr eq "5")&&($attrval >= 100)); # reallocated sector count
          $code |= 2 if (($attr eq "196")&&($attrval >= 1000)); # reallocated event count
          $code |= 2 if (($attr eq "5")&&($attrval >= 1000)); # reallocated sector count
          $code |= 2 if (($attr eq "197")&&($attrval ne "")); # current pending sector
          $code |= 2 if (($attr eq "198")&&($attrval ne "")); # offline uncorrectable
          $code |= 2 if (($attr eq "175")&&($attrval ne "")); # Bad_Cluster_Table_Count
        }

        $diskcode |= $code;

        my $bgcolour = "";
        $bgcolour = " bgcolor=yellow" if $code > 0;
        $bgcolour = " bgcolor=red"    if $code > 1;

        if (!$is_old) {
          setStatus($h, $code, "SMART");
        }

        if ($attr =~ /serno/) {
          #$line .= "<td align=right $bgcolour>$xattrval</td>";
          $line .= htmltag("td","align=right".$bgcolour,$xattrval);
        } else {
          $line .= htmltag("td","align=right".$bgcolour,$attrval);
        }
      }

      my $diskcolour = "";
      $diskcolour = " bgcolor=yellow" if $diskcode > 0;
      $diskcolour = " bgcolor=red"    if $diskcode > 1;

      my $bgcolour = "";

      $bgcolour = "bgcolor=gray" if $is_old;

      $html .= "<tr $bgcolour>";
      $html .= htmltag("td","align=center".$diskcolour,$diskmnt);
      $html .= $line;
      $html .= "</tr>\n";
    }

    $html .= "</table>\n";
  }

  return $html;
}

sub genScrub
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>Diskscrub results</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Disk"));
  $html .= htmltag("td","align=center",htmltag("b","","Size, GiB"));
  $html .= htmltag("td","align=center",htmltag("b","","Scrub Age, days"));
  $html .= htmltag("td","align=center",htmltag("b","","Slow reads"));
  $html .= htmltag("td","align=center",htmltag("b","","Read errors"));
  $html .= htmltag("td","align=center",htmltag("b","","Aborted"));
  $html .= htmltag("td","align=center",htmltag("b","","Slowest read, sec"));
  $html .= htmltag("td","align=center",htmltag("b","","AveRate, MiB/sec"));
  $html .= htmltag("td","align=center",htmltag("b","","MinRate, MiB/sec"));
  $html .= htmltag("td","align=center",htmltag("b","","MaxRate, MiB/sec"));
  $html .= htmltag("td","align=center",htmltag("b","","Scrub time, hrs"));
  $html .= htmltag("td","align=center",htmltag("b","","ScrubRate, MiB/sec"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $scrubxml = $jsondata{$h}{"diskscrub_xml"};

    if ($scrubxml) {
      my @d = splitXmlTag($scrubxml, "disk");

      foreach my $d (@d) {

        #print STDERR "[$d]\n";

        my $host = findXmlTag($d, "host");
        my $dev = findXmlTag($d, "name");
        my $size = findXmlTag($d, "size");
        my $slow =  findXmlTag($d, "countSlow");
        my $short = findXmlTag($d, "countShort");
        my $errors = findXmlTag($d, "countUnreadable");
        my $aborted = findXmlTag($d, "countAborted");
        my $minrate = findXmlTag($d, "minRate");
        my $maxrate = findXmlTag($d, "maxRate");
        my $averate = findXmlTag($d, "aveRate");
        my $scrubrate = findXmlTag($d, "effRate");
        my $slowread = findXmlTag($d, "longestReadTime");
        my $timeBegin = findXmlTag($d, "timeBegin");
        my $timeEnd   = findXmlTag($d, "timeEnd");

        $size /= 1024;
        $size /= 1024;
        $size /= 1024;

        $size = int($size);
        $slowread = sprintf("%.1f", $slowread);
        $scrubtime = sprintf("%.1f", ($timeEnd - $timeBegin)/(60*60));
        $scrubrate = sprintf("%.1f", $scrubrate/(1024*1024));
        $minrate = sprintf("%.1f", $minrate/(1024*1024));
        $maxrate = sprintf("%.1f", $maxrate/(1024*1024));
        $averate = sprintf("%.1f", $averate/(1024*1024));

        my $age = time() - $timeEnd;

        $age = sprintf("%.1f", $age/(60*60*24));

        my $disk = "$h:$dev";

        $errors = $errors + $short if $short;

        $slow   = "" unless $slow != 0;
        $errors = "" unless $errors != 0;
        $aborted = "" unless $aborted && $aborted != 0;

        my $estatus = 0;
        my $sstatus = 0;
        my $rstatus = 0;
        my $astatus = 0;

        $estatus |= 2 if $errors;
        $astatus |= 2 if $aborted;
	if (defined $slowread) {
          $sstatus |= 1 if $slowread > 20.0;
          $sstatus |= 2 if $slowread > 60.0;
        }
	if (defined $maxrate) {
          $rstatus |= 1 if $maxrate  < 10.0;
        }

	if ($errors || $aborted) {
          setStatus($h, 2, "SCRUB");
	}

        $html .= "<tr class=$class{$h}>";
        $html .= htmltag("td","align=center ".statusColour($estatus|$sstatus|$rstatus|$astatus),$disk);
        $html .= htmltag("td","align=right",$size);
        $html .= htmltag("td","align=right",$age);
        $html .= htmltag("td","align=right",$slow);
        $html .= htmltag("td","align=right ".statusColour($estatus),$errors);
        $html .= htmltag("td","align=right ".statusColour($astatus),$aborted);
        $html .= htmltag("td","align=right ".statusColour($sstatus),$slowread);
        $html .= htmltag("td","align=right",$averate);
        $html .= htmltag("td","align=right",$minrate);
        $html .= htmltag("td","align=right ".statusColour($rstatus),$maxrate);
        $html .= htmltag("td","align=right",$scrubtime);
        $html .= htmltag("td","align=right",$scrubrate);
        $html .= "</tr>\n";
      }
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub genZfs
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>ZFS configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","zpool status"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $conf = $jsondata{$h}{"zpool_status"};

    if (defined $conf && length $conf > 1) {
      my $zred = "";
      if ($conf =~ /DEGRADED/) {
        $zred = "bgcolor=red"; 
        setStatus($h, 2, "ZFS", 1);
      }
      if ($conf =~ /REMOVED/) {
        $zred = "bgcolor=red"; 
        setStatus($h, 2, "ZFS", 1);
      }
      if ($conf =~ /UNAVAILABLE/) {
        $zred = "bgcolor=red"; 
        setStatus($h, 2, "ZFS", 1);
      }
      if ($conf =~ /action:/) {
        $zred = "bgcolor=red"; 
        setStatus($h, 2, "ZFS", 1);
      }

      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=center", $h);
      $html .= htmltag("td","align=left $zred", htmltag("pre","",$conf));
      $html .= "</tr>\n";
    }
  }

  $html .= "</table>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","zpool list"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $conf = $jsondata{$h}{"zpool_list"};

    if (defined $conf && length $conf > 1) {
      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=center", $h);
      $html .= htmltag("td","align=left", htmltag("pre","",$conf));
      $html .= "</tr>\n";
    }
  }

  $html .= "</table>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","zpool list -v"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $conf = $jsondata{$h}{"zpool_list_v"};

    if (defined $conf && length $conf > 1) {
      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=center", $h);
      $html .= htmltag("td","align=left", htmltag("pre","",$conf));
      $html .= "</tr>\n";
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub genMdstat
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>/proc/mdstat configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","/proc/mdstat"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $conf = $jsondata{$h}{"mdstat"};

    if (defined $conf) {
      next unless $conf =~ /raid/;

      $conf =~ s/\n/\<br\>/mg;
      $conf =~ s/ /\&nbsp\;/mg;
      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=center",$h);
      $html .= htmltag("td","align=left",$conf);
      $html .= "</tr>\n";
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub toGiB
  {
    my $KiB = 1024;
    my $val = shift @_;
    return 0 if ! defined $val;
    return sprintf("%.1f", $val/($KiB*$KiB));
  }

sub genMd
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>md raid configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","Mount"));
  $html .= htmltag("td","align=center",htmltag("b","","Raid"));
  $html .= htmltag("td","align=center",htmltag("b","","State"));
  $html .= htmltag("td","align=center",htmltag("b","","Degraded"));
  $html .= htmltag("td","align=center",htmltag("b","","Bitmap"));
  $html .= htmltag("td","align=center",htmltag("b","","Size, GiB"));
  $html .= htmltag("td","align=center",htmltag("b","","Disk size, GiB"));
  $html .= htmltag("td","align=center",htmltag("b","","Mismatch_cnt"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};

    my $mounts = $jsondata{$h}{"mounts"};
    my $swaps  = $jsondata{$h}{"swaps"};

    my $jconf = $jsondata{$h}{"sys_block"};
    if (defined $jconf) {
      foreach my $dev (sort keys %{$jconf}) {
        next unless $dev =~ /^md/;

        my $size = $jsondata{$h}{"sys_block"}{$dev}{"size"};
        my $component_size = $jsondata{$h}{"sys_block"}{$dev}{"md_component_size"};

        next unless $size > 0;

        my $degraded = $jsondata{$h}{"sys_block"}{$dev}{"md_degraded"};
        my $mismatch_cnt = $jsondata{$h}{"sys_block"}{$dev}{"md_mismatch_cnt"};
        my $bitmap   = $jsondata{$h}{"sys_block"}{$dev}{"md_bitmap_chunksize"};
        my $level    = $jsondata{$h}{"sys_block"}{$dev}{"md_level"};
        my $state    = $jsondata{$h}{"sys_block"}{$dev}{"md_array_state"};

        #die "HERE: $h: $jconf $dev $size $component_size $degraded $mismatch_cnt $bitmap\n";

        $bitmap = "yes" if $bitmap > 0;
            
        my $mnt;
        my $swap = 0;
            
        if ($mounts) {
          my @m = split(/\n/, $mounts);
          @m = grep(/$dev /, @m);
          if ($m[0]) {
            my ($xdev, $xmnt, $xfstype) = split(/\s+/, $m[0]);
            $mnt .= "$xmnt ($xfstype) ";
          }
        }
            
        if ($swaps) {
          my @s = split(/\n/, $swaps);
          @s = grep(/$dev/, @s);
          if ($s[0]) {
            $mnt .= "swap ";
            $swap = 1;
          }
        }
            
        my $dstatus = 0;
        my $mstatus = 0;

        $dstatus |= 2 if ($degraded && $degraded != 0);
        #$mstatus |= 1 if ($mismatch_cnt && $mismatch_cnt != 0 && $swap);
        #$mstatus |= 2 if ($mismatch_cnt && $mismatch_cnt != 0 && !$swap);

        my $xstatus = $dstatus|$mstatus;

        setStatus($h, $xstatus, "MD", 1);

        $html .= "<tr class=$class{$h}>";
        $html .= htmltag("td","align=center ".statusColour($xstatus),"$h:/dev/$dev");
        $html .= htmltag("td","align=center", $mnt);
        $html .= htmltag("td","align=right",  $level);
        $html .= htmltag("td","align=center", $state);
        $html .= htmltag("td","align=right ".statusColour($dstatus), $degraded);
        $html .= htmltag("td","align=center", $bitmap);
        $html .= htmltag("td","align=right", toGiB($size/2.0));
        $html .= htmltag("td","align=right", toGiB($component_size));
        $html .= htmltag("td","align=right ".statusColour($mstatus), $mismatch_cnt);
        $html .= "</tr>\n";
      }
    }

    my $conf = $jsondata{$h}{"xml_md"};
    if (defined $conf) {
      my $names = findXmlTag($conf, "names");
      foreach my $dev (sort split(/\n/, $names)) {
        my $data = findXmlTag($conf, $dev);

        next if ! $data;

        my $size = findXmlTag($data, "size");

        next unless $size > 0;

        $data =~ s/\n/\<br\>/mg;
        $data =~ s/ /\&nbsp\;/mg;

        my $degraded = findXmlTag($data, "degraded");
        my $mismatch_cnt = findXmlTag($data, "mismatch_cnt");
        my $bitmap = $data =~ /bitmap_set_bits/;

        $bitmap = "maybe" if $bitmap;
            
        my $mnt;
        my $swap = 0;
            
        if ($mounts) {
          my @m = split(/\n/, $mounts);
          @m = grep(/$dev/, @m);
          if ($m[0]) {
            my ($xdev, $xmnt, $xfstype) = split(/\s+/, $m[0]);
            $mnt .= "$xmnt ($xfstype) ";
          }
        }
            
        if ($swaps) {
          my @s = split(/\n/, $swaps);
          @s = grep(/$dev/, @s);
          if ($s[0]) {
            $mnt .= "swap ";
            $swap = 1;
          }
        }
            
        my $dstatus = 0;
        my $mstatus = 0;

        $dstatus |= 2 if ($degraded && $degraded != 0);
        #$mstatus |= 1 if ($mismatch_cnt && $mismatch_cnt != 0 && $swap);
        #$mstatus |= 2 if ($mismatch_cnt && $mismatch_cnt != 0 && !$swap);

        my $xstatus = $dstatus|$mstatus;

        setStatus($h, $xstatus, "MD", 1);

        $html .= "<tr class=$class{$h}>";
        $html .= htmltag("td","align=center ".statusColour($xstatus),"$h:/dev/$dev");
        $html .= htmltag("td","align=center", $mnt);
        $html .= htmltag("td","align=right",findXmlTag($data, "level"));
        $html .= htmltag("td","align=center",findXmlTag($data, "array_state"));
        $html .= htmltag("td","align=right ".statusColour($dstatus), $degraded);
        $html .= htmltag("td","align=center", $bitmap);
        $html .= htmltag("td","align=right",toGiB($size/2.0));
        $html .= htmltag("td","align=right",toGiB(findXmlTag($data, "component_size")));
        $html .= htmltag("td","align=right ".statusColour($mstatus), $mismatch_cnt);
        $html .= "</tr>\n";
      }
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub genBoot
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>boot disk configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Disk"));
  $html .= htmltag("td","align=center",htmltag("b","","MBR"));
  $html .= htmltag("td","align=center",htmltag("b","","Partitions"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $conf = $jsondata{$h}{"xml_boot"};

    if (defined $conf) {
      my $names = findXmlTag($conf, "names");
      foreach my $dev (sort split(/\n/, $names)) {
        next if $dev =~ /^loop/;
        next if $dev =~ /^nbd/;
        next if $dev =~ /^fd/;
        next if $dev =~ /^md/;

        my $data = findXmlTag($conf, $dev);
        my $xdata = $data;

        next if ! $data;

        $data =~ s/\n/ /mg;

        $data =~ s/(1024\+0 records .*$)//;
        $data =~ s/(0\+0 records .*$)//;
                                    
        #$data =~ s/ /\&nbsp\;/mg;

        my $type = "";

        if ($data =~ /GRUB/) {
          $type = "GRUB";
        } elsif ($data =~ /Operating system loading error/) {
          $type = "SYSLINUX MBR";
        } elsif ($data =~ /EXTLINUX/) {
          $type = "EXTLINUX";
        } elsif ($data =~ /Loading stage1.5/) {
          $type = "GRUB stage 1.5";
        } else {
          $type = "$data";
        }

        my $partitions = "";

        my $parts = findXmlTag($xdata, "partitions");

        if ($parts) {
          foreach my $p (sort split(/\n/, $parts)) {
            next unless length $p > 1;
            my $pdata = findXmlTag($xdata, $p);

            my $ptype = "";
                    
            if ($pdata =~ /GRUB/) {
              $ptype = "GRUB";
            } elsif ($pdata =~ /Operating system loading error/) {
              $ptype = "SYSLINUX MBR";
            } elsif ($pdata =~ /EXTLINUX/) {
              $ptype = "EXTLINUX";
            } elsif ($pdata =~ /Loading stage1.5/) {
              $ptype = "GRUB stage 1.5";
            } else {
              #$ptype = "$pdata";
            }

            $partitions .= "$p($ptype) " if length $ptype > 0;
          }
        }

        $html .= "<tr class=$class{$h}>";
        $html .= htmltag("td", "align=center", "$h:/dev/$dev");
        $html .= htmltag("td", "align=center", $type);
        $html .= htmltag("td", "align=center", $partitions);

        #if (length $type > 1) {
        #  $html .= htmltag("td", "align=center", "");
        #} else {
        #  $html .= htmltag("td", "align=center", $data);
        #}
              
        $html .= "</tr>\n";
      }
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub yum_lock_ok
{
  my $h = shift @_;

  my $conf = $jsondata{$h}{"xml_yum"};

  if (defined $conf) {
    my $yumautoupdate = findXmlTag($conf, "yumautoupdate");
    my $yumautoupdate2 = findXmlTag($conf, "yumautoupdate2");
    my $yumdotcron = findXmlTag($conf, "yumdotcron");
    my $yumdashcron = findXmlTag($conf, "yumdashcron");

    sub lock_ok
      {
        my $v = shift @_;
        #print $v."\n";
        return 0 if ($v =~ /\/var\/lock.*No such file/);
        #die "[$r] Here!";
        return 1;
      }

    sub all_found
      {
        my $v = shift @_;
        #print $v."\n";
        return 0 if ($v =~ /No such file/);
        #die "[$r] Here!";
        return 1;
      }

    if ($yumautoupdate2) {
      my $ok1 = all_found($yumautoupdate2);
      #print "yumautoupdate2: [$yumautoupdate2] $ok1\n";

      if ($ok1) {
        my $sysconfig = findXmlTag($conf, "yumautoupdate2sysconfig");

        if ($sysconfig) {
          my $ok2 = ($sysconfig =~ /^ENABLED=\"true\"/m);
          #print "sysconfig [$sysconfig\n] [$ok2]\n";
          #print "[$ok2]\n";
          if ($ok2) {
            return 1;
          }
        }
      }
    }

    return 1 if lock_ok($yumautoupdate) || lock_ok($yumdotcron) || lock_ok($yumdashcron);
  }

  return 0;
}

sub genYum
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>yum configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","Yum-conf"));
  $html .= htmltag("td","align=center",htmltag("b","","Yum-cron"));
  #$html .= htmltag("td","align=center",htmltag("b","","Nightly"));
  $html .= htmltag("td","align=center",htmltag("b","","Repos"));
  $html .= htmltag("td","align=center",htmltag("b","","Repos (no gpgcheck)"));
  $html .= htmltag("td","align=center",htmltag("b","","Repos (not enabled)"));
  $html .= "</tr>\n";


  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $conf = $jsondata{$h}{"xml_yum"};

    if (defined $conf) {
      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td", "align=center", "$h");

      my $yumconf = "";
      my $yumcron = "";

      my $xcolour = "";

      my @rpms;
      my $splitrpms = $jsondata{$h}{"split_rpms"};
      @rpms = @{$splitrpms} if $splitrpms;

      my @yumrpms = grep(/yum/, @rpms);

      $yumconf = join(" ", grep(/conf/, @yumrpms));

      $yumcron .= join(" ", grep(/cron/, @yumrpms));
      $yumcron .= join(" ", grep(/autoupdate/, @yumrpms));

      if (length $yumconf < 1) {
        $yumconf = join(" ", grep(/sl-release-\d/, @rpms));
      }

      $yumconf =~ s/.noarch//g;
      $yumcron =~ s/.noarch//g;
      $yumconf =~ s/.x86_64//g;
      $yumcron =~ s/.x86_64//g;
      $yumconf =~ s/.i686//g;
      $yumcron =~ s/.i686//g;
      $yumconf =~ s/.i386//g;
      $yumcron =~ s/.i386//g;

      my $yumautoupdate = findXmlTag($conf, "yumautoupdate");
      my $yumdotcron = findXmlTag($conf, "yumdotcron");
      my $yumdashcron = findXmlTag($conf, "yumdashcron");

      sub isInstalled
        {
          my $x = shift @_;
          my @x = split(/\n/, $x);
          my $yes = 0;
          foreach my $xxx (@x) {
            next if length $xxx < 1;
            $yes = 1 unless $xxx =~ /No such file/;
            #print "$yes [$xxx]\n";
          }
          return $yes;
        }

      sub isOkey
        {
          my $x = shift @_;
          my @x = split(/\n/, $x);
          my $yes = 1;
          foreach my $xxx (@x) {
            next if length $xxx < 1;
            $yes = 0 unless $xxx =~ /^\-r/;
            #print "$yes [$xxx]\n";
          }
          return $yes;
        }

      my $nightly = "";

      if (isInstalled($yumautoupdate)) {
        if (isOkey($yumautoupdate)) {
          $nightly .= "yum-autoupdate ";
        } else {
          $nightly .= "BROKEN-yum-autoupdate ";
        }
      }

      if (isInstalled($yumdotcron)) {
        if (isOkey($yumdotcron)) {
          $nightly .= "yum.cron ";
        } else {
          $nightly .= "BROKEN-yum.cron ";
        }
      }

      if (isInstalled($yumdashcron)) {
        if (isOkey($yumdashcron)) {
          $nightly .= "yum-cron ";
        } else {
          $nightly .= "BROKEN-yum-cron ";
        }
      }

      my @repos_on;
      my @repos_nogpg;
      my @repos_off;

      my $rr = findXmlTag($conf, "repos");
      my @repos = split(/\<repo/, $rr);
      foreach $r (sort @repos) {
        $r =~ /file\=\"(.*)\"\>(.*)\<\/repo\>/s;
        my $f = $1;
        my $c = $2;

        next unless $f;
        next unless $c;
        next unless $f =~ /repo$/;

        #print "[$r][$f][$c]\n";

        $c =~ s/^#.*$//mg; # remove comment lines
        $c =~ s/^\s+$//mg; # remove blank lines

        $c =~ s/^.*?\[//s;

        my @x = split(/\[/, $c);
        foreach $x (sort @x) {

          $x =~ /^(.*)\]/;
          my $name = $1;

          my ($enabled) = ($x =~ /enabled.*\=.*(\d+)/);
          my ($gpgcheck) = ($x =~ /gpgcheck.*\=.*(\d+)/);

          $enabled = 1  if !defined $enabled;
          $gpgcheck = 0 if !defined $gpgcheck;

          if (!$enabled) {
            push @repos_off, $name;
          } elsif (!$gpgcheck) {
            push @repos_nogpg, $name;
            setStatus($h, 2, "YUM", 1);
            $xcolour = "bgcolor=red";
          } else {
            push @repos_on, $name;
          }

          #print "[$f][$x][$enabled][$gpgcheck]\n";
        }
      }

      #my $repos_off = join("<br>", sort @repos_off);
      my $repos_off = @repos_off;
      my $repos_nogpg = join("<br>", sort @repos_nogpg);
      my $repos_on = join("<br>", sort @repos_on);
          
      $yumcron = $nightly if (length $yumcron < 1);

      $html .= htmltag("td", "align=center", $yumconf);
      $html .= htmltag("td", "align=center", $yumcron);
      #$html .= htmltag("td", "align=center", $nightly);
      $html .= htmltag("td", "align=center", $repos_on);
      $html .= htmltag("td $xcolour", "align=center", $repos_nogpg);
      $html .= htmltag("td", "align=center", $repos_off);
      $html .= "</tr>\n";

      #die "Here!";
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub genNisconfig
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>NIS configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("th","align=center","Hostname");
  $html .= htmltag("th","align=center","Domain");
  $html .= htmltag("th","align=center","ypserv");
  $html .= htmltag("th","align=center","ypwhich");
  $html .= htmltag("th","align=center","/etc/yp.conf");
  $html .= htmltag("th","align=center","/var/yp/securenets");
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $domain  = $jsondata{$h}{"yp_domainname"};
    my $ypconf  = $jsondata{$h}{"yp_etc_yp_conf"};
    my $ypwhich = $jsondata{$h}{"yp_ypwhich"};
    my $ypserv  = $jsondata{$h}{"yp_ypserv_pid"};
    my $securenets = $jsondata{$h}{"yp_securenets"};

    if ($domain) {
      undef $domain if $domain eq "(none)";
    }

    my $yp;

    if (defined $ypconf) {
      my @ypconf = grep(!/^#/, split(/\n/, $ypconf));
      @ypconf = grep(!/^$/, @ypconf);
      @ypconf = grep(!/^\s+$/, @ypconf);

      #my $ypconf = join("\n", @ypconf);
      #my ($ypdomain) = ($ypconf =~ m/domain\s+(\S+)/g);

      #print "[$h] [$ypdomain] [$ypconf]\n";

      #if (defined $ypdomain) {
      #  my $broadcast = "";
      #  $broadcast = " broadcast" if $ypconf =~ /broadcast/;
      #
      #  my $server = "";
      #  $server = " server $1" if $ypconf =~ /server\s+(\S+)/;
      #
      #  $yp = "$server$broadcast";
      #}
      #
      #$yp = $ypconf;

      #$yp =~ s/\n/<br>\n/g;

      $yp = join("<br>\n", @ypconf);

      #print "[$h] [$yp]\n";
    }

    $html .= "<tr class=$class{$h}>";
    $html .= htmltag("td","align=center",$h);
    if ($domain || $ypwhich || $yp) {
      $domain = "" if ! $domain;

      $html .= htmltag("td", "align=center style=\"white-space: nowrap\"", $domain);
      if (!defined $ypserv || (length($ypserv) < 1)) {
        $html .= htmltag("td", "align=center", "");
      } elsif ($ypserv =~ /\d+/) {
        $html .= htmltag("td", "align=center", $ypserv);
      } else {
        $html .= htmltag("td","align=center bgcolor=red","missing [$ypserv]");
        setStatus($h, 2, "NIS", 1);
      }

      if ($ypwhich) {
        my $zred = "";
        if ($ypwhich =~ /Can\'t communicate with ypbind/) {
          $zred = "bgcolor=red"; 
          setStatus($h, 2, "NIS", 1);
        }
        $html .= htmltag("td","align=center $zred",$ypwhich);
      } else {
        $html .= htmltag("td","align=center bgcolor=red","not bound");
        setStatus($h, 2, "NIS", 1);
      }

      $html .= htmltag("td","align=center style=\"white-space: nowrap\"",$yp)      if defined $yp;
      $html .= htmltag("td","align=center bgcolor=red","error")  if ! defined $yp;
      if (! defined $yp) {
        setStatus($h, 2, "NIS", 1);
      }

      if (my $ret = $securenets) {
        $html .= htmltag("td","align=center","yes");
      } else {
        $html .= htmltag("td","align=center","-");
      }
    } else {
      $html .= htmltag("td","align=center","");
      $html .= htmltag("td","align=center","");
      $html .= htmltag("td","align=center","");
      $html .= htmltag("td","align=center","");
      $html .= htmltag("td","align=center","");
    }
    $html .= "</tr>\n";
  }

  $html .= "</table>\n";

  return $html;
}

sub genNetconfig
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>Network configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<thead>";
  $html .= "<tr>";
  $html .= htmltag("th","align=center","Hostname");
  $html .= htmltag("th","","Link");
  $html .= htmltag("th","","sysconfig");
  $html .= htmltag("th","","MAC");
  $html .= htmltag("th","","IPv4");
  $html .= htmltag("th","","routes");
  $html .= htmltag("th","","DNS");
  $html .= "</tr>\n";
  $html .= "</thead>";
  $html .= "<tbody>";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $xlink = "";

    if (1) {
      my $conf = $jsondata{$h}{"xml_ethtool"};

      if ($conf) {
        my $names = findXmlTag($conf, "names");
        foreach my $dev (sort split(/\n/, $names)) {
          my $data = findXmlTag($conf, $dev);
              
          next if ! $data;

          #print "$data\n";

          $data =~ /Speed\:\s+(.*)/;

          my $speed = $1;

          next unless $speed;

          $xlink .= "$dev:&nbsp;$speed<br>";

          #die "Here!";
        }
      }
    }

    #print "$h [$xlink]\n";

    if (length($xlink) < 1) {
      my $link = $jsondata{$h}{"xml_miitool"};
      $link = "(unknown)" if ! defined $link;
      
      foreach my $linkk (sort split(/\n/,$link)) {
        #$xlink .= "100-FD" if $link eq "eth0: negotiated 100baseTx-FD, link ok";
        #$link = "100-FD-FC" if $link eq "eth0: negotiated 100baseTx-FD flow-control, link ok";
          
        $linkk = "$1: no link" if $linkk =~ /(.*):.*no link/;
        $linkk =~ s/negotiated//;
        $linkk =~ s/baseTx-/ /;
        $linkk =~ s/link ok//;
        $linkk =~ s/flow-control/FC/;
        $linkk =~ s/full duplex/FD/;
        $linkk =~ s/\,/ /g;
        $linkk =~ s/\s+/&nbsp;/g;
          
        $xlink .= "$linkk<br>";
      }
    }

    my $dhcp;
    my $ifcfg = $jsondata{$h}{"xml_ifcfg"};
    my $eth0 = $jsondata{$h}{"xml_ifcfgeth0"};

    if (defined $ifcfg) {
      my @data = split(/<file>/, $ifcfg);
      foreach my $d (sort @data) {
        my $name = findXmlTag($d, "name");
        my $dddd = findXmlTag($d, "data");
        next unless $name;
        next unless $dddd;
        $dhcp .= $name . ": ";
        $dhcp .= "off "    if $dddd =~ /ONBOOT=.*no.*/;
        $dhcp .= "dhcp "   if $dddd =~ /BOOTPROTO=.*dhcp.*/;
        $dhcp .= "static " if $dddd =~ /IPADDR=/;
        $dhcp .= "NM " if $dddd =~ /NM_CONTROLLED=.*yes.*/;
        $dhcp .= "<br>";
      }
    } elsif (defined $eth0) {
      #$dhcp = "dhcp" if $eth0 =~ /BOOTPROTO=.*dhcp.*/;
      #$dhcp = "static" if $eth0 =~ /IPADDR=/;
      $dhcp = "off" if $eth0 =~ /ONBOOT=.*no.*/;
      $dhcp = "dhcp" if $eth0 =~ /BOOTPROTO=.*dhcp.*/;
      $dhcp = "static" if $eth0 =~ /IPADDR=/;
      $dhcp = "NM-$dhcp" if $eth0 =~ /NM_CONTROLLED=.*yes.*/;

      $dhcp = "eth0-$dhcp";
    } else {
      $dhcp = "???";
    }

    my $resolv = $jsondata{$h}{"etc_resolv_conf"};
    my $dns;
    if (defined $resolv) {
      $dns = "";
      foreach my $line (split (/\n/,$resolv)) {
        next if ! ($line =~ /^nameserver\s+(\S+)/);
        $dns .= $1;
        $dns .= "<br>";
      }
    }

    my $ifconfig = "";
    my $xifconfig = $jsondata{$h}{"ifconfig"};
    if (defined $xifconfig) {
      my @x = split(/\n\n/,$xifconfig);

      foreach my $x (sort @x) {
        my ($name) = split(/\s+/,$x);

        next if $name =~ /^lo/;

        my $ip = "";
        my $mac = "";

        $ip  = $1 if ($x =~ /inet addr:(.*?)\s/);
        $mac = $1 if ($x =~ /HWaddr (.*?)\s/);

        $ifconfig .= "$name: $mac $ip<br>";
      }
    }

    my $ipr = $jsondata{$h}{"ip_r"};
    my $ipa = $jsondata{$h}{"ip_a"};
    my $ethtool = $jsondata{$h}{"ethtool"};
    if ($ipr && $ipa & $ethtool) {
      my %devs;
      my %links;
      foreach my $k (sort keys %{$ethtool}) {
        my $e = $jsondata{$h}{"ethtool"}{$k}{"ethtool"};
        my $eP = $jsondata{$h}{"ethtool"}{$k}{"ethtool_P"};
        my $ei = $jsondata{$h}{"ethtool"}{$k}{"ethtool_i"};
        my ($link) = ($e =~ m/Link detected: (.*?)$/m);
        #print "[$h] [$k] [$e] [$eP] [$ei] [$link]\n" if $h eq "daqubuntu";
        if ($link && $link eq "yes") {
          my ($speed) = ($e =~ m/Speed: (.*?)$/m);
          my ($duplex) = ($e =~ m/Duplex: (.*?)$/m);
          if ($speed) {
            #print "[$h] [$k] [$e] [$eP] [$ei] [$speed]\n" if !$speed;
            $speed .= "-FD" if ($duplex eq "Full");
            $devs{$k} = 1;
            $links{$k} = "$speed";
          } else {
            $devs{$k} = 1;
            $links{$k} = "no speed";
          }
        } else {
          $devs{$k} = 1;
          $links{$k} = "no link";
        }
      }

      my %addrs;
      my %macs;
      my @ipa = split(/^\d+/m, $ipa);
      foreach my $a (sort @ipa) {
        next if length($a) < 1;
        my ($dev) = ($a =~ m/:\s+(.*?):/g);
        my ($ipv4) = ($a =~ m/inet (.*?) /g);
        my ($mac) = ($a =~ m/link\/ether (.*?) /g);
        my ($mtu) = ($a =~ m/mtu (.*?) /g);
        #print "[$h] [$a] [$dev] [$ipv4]\n" if !$dev;
        $devs{$dev} = 1;
        $mac = "no mac" if !$mac;
        $ipv4 = "no ipv4" if !$ipv4;
        $addrs{$dev} .= "$ipv4 ($mtu)<br>";
        $macs{$dev} .= "$mac<br>";
      }

      my %routes;
      my @ipr = split(/\n/, $ipr);
      foreach my $r (sort @ipr) {
        if ($r =~ m/via/) {
          my ($route) = ($r =~ m/(.*?) /);
          my ($via) = ($r =~ m/via (.*?) /);
          my ($dev) = ($r =~ m/dev (.*?) /);
          my ($metric) = ($r =~ m/metric (.*) /);
          #print "[$h] [$r] [$metric]\n" if !$metric;
          my $r = "$route";
          $r .= " ($metric)" if $metric;
          $r .= "<br>";
          $routes{$dev} .= $r;
          $devs{$dev} = 1;
        } else {
          my ($net) = ($r =~ m/^(.*?) /);
          my ($dev) = ($r =~ m/dev (.*?) /);
          my ($src) = ($r =~ m/src (.*?) /);
          my ($metric) = ($r =~ m/metric (.*?) /);
          my $r = "$net";
          $r .= " ($metric)" if $metric;
          $r .= "<br>";
          $routes{$dev} .= $r;
          $devs{$dev} = 1;
        }
      }

      foreach my $dev (sort keys %devs) {
        $html .= "<tr class=$class{$h}>";
        $html .= htmltag("td", "align=left", "$h:$dev");
        $html .= htmltag("td", "align=center style=\"white-space: nowrap\"", $links{$dev});
        #$html .= htmltag("td","",$dhcp) if defined $dhcp;
        #$html .= htmltag("td","bgcolor=red","error")  if ! defined $dhcp;
        #if (! defined $dhcp) {
        #  setStatus($h, 2, "NET", 1);
        #}
        $html .= htmltag("td", "", "???");
        $html .= htmltag("td", "style=\"white-space: nowrap\"", $macs{$dev});
        $html .= htmltag("td", "style=\"white-space: nowrap\"", $addrs{$dev});
        $html .= htmltag("td", "style=\"white-space: nowrap\"", $routes{$dev});
        $html .= htmltag("td", "", $dns) if defined $dns;
        $html .= htmltag("td", "bgcolor=red","error")  if ! defined $dns;
        if (! defined $dns) {
          setStatus($h, 2, "NET", 1);
        }
        $html .= "</tr>\n";
      }
    } else {
      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=left",$h);
      $html .= htmltag("td","",$xlink);
      $html .= htmltag("td","",$dhcp) if defined $dhcp;
      $html .= htmltag("td","bgcolor=red","error")  if ! defined $dhcp;
      if (! defined $dhcp) {
        setStatus($h, 2, "NET", 1);
      }
      $html .= htmltag("td","",$ifconfig);
      $html .= htmltag("td","", "no ip_a");
      $html .= htmltag("td","", "no ip_r");
      $html .= htmltag("td","",$dns) if defined $dns;
      $html .= htmltag("td","bgcolor=red","error")  if ! defined $dns;
      if (! defined $dns) {
        setStatus($h, 2, "NET", 1);
      }
      $html .= "</tr>\n";
    }
  }

  $html .= "</tbody>";
  $html .= "</table>\n";

  return $html;
}

sub genStorage
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>Block device storage configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","Device"));
  $html .= htmltag("td","align=center",htmltag("b","","Size"));
  $html .= htmltag("td","align=center",htmltag("b","","Removable/RO"));
  $html .= htmltag("td","align=center",htmltag("b","","Identity"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $sysblock = $jsondata{$h}{"sys_block"};
    next unless $sysblock;
    my $devs = "";
    my $sizes = "";
    my $rros = "";
    my $ids = "";
    foreach my $dev (sort keys %{$sysblock}) {
      next if $dev =~ m/^loop/;
      next if $dev =~ m/^ram/;
      next if length $dev < 1;
      my $size = $jsondata{$h}{"sys_block"}{$dev}{"size"};
      my $removable = $jsondata{$h}{"sys_block"}{$dev}{"removable"};
      my $ro = $jsondata{$h}{"sys_block"}{$dev}{"ro"};
      #my $vendor = $jsondata{$h}{"sys_block"}{$dev}{"vendor"};
      my $model = $jsondata{$h}{"sys_block"}{$dev}{"model"};
      my $slaves = $jsondata{$h}{"sys_block"}{$dev}{"slaves"};

      #print "[$h] [$dev] [$size]\n";

      $size = sprintf("%.0f GB", ($size*512)/(1000*1000*1000)) if $size;
          
      $devs  .= "$dev<br>";
      $sizes .= "$size<br>";
      $rros  .= "$removable/$ro<br>";
      if ($model) {
        $ids   .= "$model<br>";
      } elsif ($slaves) {
        $ids   .= "$slaves<br>";
      } else {
        $ids   .= "???<br>";
      }
    }

    next unless $devs;
        
    $html .= "<tr class=$class{$h}>";
    $html .= htmltag("td", "align=center", $h);
    $html .= htmltag("td", "align=center",$devs);
    $html .= htmltag("td", "align=right", $sizes);
    $html .= htmltag("td", "align=center", $rros);
    $html .= htmltag("td", "align=left", $ids);
    $html .= "</tr>\n";
  }

  $html .= "</table>\n";

  $html .= "<h2>md raid storage configuration</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","Mount"));
  $html .= htmltag("td","align=center",htmltag("b","","Raid"));
  $html .= htmltag("td","align=center",htmltag("b","","Version"));
  $html .= htmltag("td","align=center",htmltag("b","","State"));
  $html .= htmltag("td","align=center",htmltag("b","","Degraded"));
  $html .= htmltag("td","align=center",htmltag("b","","Bitmap, MiB"));
  $html .= htmltag("td","align=center",htmltag("b","","Size, GiB"));
  $html .= htmltag("td","align=center",htmltag("b","","Disk size, GiB"));
  $html .= htmltag("td","align=center",htmltag("b","","Mismatch_cnt"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my $mounts = $jsondata{$h}{"mounts"};
    my $swaps  = $jsondata{$h}{"swaps"};
    my $sysblock = $jsondata{$h}{"sys_block"};
    next unless $sysblock;
    foreach my $dev (sort keys %{$sysblock}) {
      next unless $dev =~ m/^md/;
      my $size = $jsondata{$h}{"sys_block"}{$dev}{"size"};
      next unless $size > 0;

      my $degraded = $jsondata{$h}{"sys_block"}{$dev}{"md_degraded"};
      my $mismatch_cnt = $jsondata{$h}{"sys_block"}{$dev}{"md_mismatch_cnt"};
      my $bitmap = $jsondata{$h}{"sys_block"}{$dev}{"md_bitmap_chunksize"};
      my $component_size = $jsondata{$h}{"sys_block"}{$dev}{"md_component_size"};
      my $version = $jsondata{$h}{"sys_block"}{$dev}{"md_metadata_version"};
      my $level = $jsondata{$h}{"sys_block"}{$dev}{"md_level"};
      my $state = $jsondata{$h}{"sys_block"}{$dev}{"md_array_state"};
      
      my $mnt;
      my $swap = 0;
      
      if ($mounts) {
        my @m = split(/\n/, $mounts);
        @m = grep(/$dev/, @m);
        if ($m[0]) {
          my ($xdev, $xmnt, $xfstype) = split(/\s+/, $m[0]);
          $mnt .= "$xmnt ($xfstype) ";
        }
      }
      
      if ($swaps) {
        my @s = split(/\n/, $swaps);
        @s = grep(/$dev/, @s);
        if ($s[0]) {
          $mnt .= "swap ";
          $swap = 1;
        }
      }
            
      my $dstatus = 0;
      my $mstatus = 0;

      $dstatus |= 2 if ($degraded && $degraded != 0);
      #$mstatus |= 1 if ($mismatch_cnt && $mismatch_cnt != 0 && $swap);
      #$mstatus |= 2 if ($mismatch_cnt && $mismatch_cnt != 0 && !$swap);
      
      my $xstatus = $dstatus|$mstatus;

      setStatus($h, $xstatus, "MD", 1);

      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td", "align=center ".statusColour($xstatus),"$h:/dev/$dev");
      $html .= htmltag("td", "align=center", $mnt);
      $html .= htmltag("td", "align=right", $level);
      $html .= htmltag("td", "align=right", $version);
      $html .= htmltag("td", "align=center", $state);
      $html .= htmltag("td", "align=right ".statusColour($dstatus), $degraded);
      $html .= htmltag("td", "align=center", toGiB($bitmap));
      $html .= htmltag("td", "align=right", toGiB($size/2.0));
      $html .= htmltag("td", "align=right", toGiB($component_size));
      $html .= htmltag("td", "align=right ".statusColour($mstatus), $mismatch_cnt);
      $html .= "</tr>\n";
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub genRpm
{
  my $group = shift @_;
  my %defdata;

  #foreach my $os (sort keys %hosts)
  #  {
  #    my $reqrpmlist = readFile("$confNodes/../$os-rpms.txt");
  #    $defdata{$os}{"rpm1"}      = readRpms($reqrpmlist);
  #    $defdata{$os}{"rpms"}      = makeref(split(/\n/,$reqrpmlist));
  #    $defdata{$os}{"chkconfig"} = makeref(split(/\n/,readFile("$confNodes/../$os-chkconfig.txt")));
  #    #print "def data: ".join("X",@{$defdata{$os}{"rpms"}})."\n";
  #  }

  my %z_rpm_os_host;
  my %z_host_rpm;
  my %z_os_rpm;
  my %z_os_rpm_common;
  my %z_host_rpm_missing;

  my %zos;
  my %zhosts;

  foreach my $os (sort keys %osdata) {
    my $xallhosts = $osdata{$os}{"hosts"};
    foreach my $h (sort @{$xallhosts}) {
      next unless $jsondata{$h}{"groups"}{$group};
      my $xos = $os;
      $xos .= "-stale" if $stale{$h};

      $zos{$xos} = $os;
      $zhosts{$xos}{$h} = 1;

      foreach my $rpm (@{$jsondata{$h}{"split_rpms"}}) {
        $z_rpm_os_host{$rpm}{$os}{$h} = 1;
        $z_host_rpm{$h}{$rpm}   = 1;
        $z_os_rpm{$xos}{$rpm} = 1;
      }
    }
  }

  my $html;

  $html .= htmltag("h2","","Packages by OS")."\n";
  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";

  $html .= "<tr>";
  $html .= htmltag("th","","OS");
  $html .= htmltag("th","","Count");
  $html .= htmltag("th","","All");
  $html .= htmltag("th","","Common");
  $html .= htmltag("th","","Extra");
  $html .= htmltag("th","","Missing");
  $html .= "</tr>\n";

  foreach my $xos (sort keys %zos) {
    if ($xos =~ /stale/) {
      $html .= "<tr bgcolor=$xgray>";
    } else {
      $html .= "<tr>";
    }
    $html .= htmltag("td","",$xos);

    my @rpm_all;
    my @rpm_common;
    my @rpm_extra;
    my @rpm_missing;

    my $os = $zos{$xos};
    my @zallhosts = keys %{$zhosts{$xos}};

    foreach my $rpm (sort keys %{$z_os_rpm{$xos}}) {
      my @inst_hosts = sort keys %{$z_rpm_os_host{$rpm}{$xos}};
      my $inst_hosts = join(" ",@inst_hosts);
      push @rpm_all,"$rpm: ".$inst_hosts;

      my $flag = 1;
      my @missing_hosts;
      foreach my $h (@zallhosts) {
        #print "[$group][$h][$rpm][@zallhosts][$z_rpm_os_host{$rpm}{$xos}{$h}]\n" if $h eq "titan06" && $group eq "titan";
        if (scalar keys %{$z_host_rpm{$h}} < 1) {
          #print "no RPMs in $h\n";
          next;
        }
        if (! defined $z_rpm_os_host{$rpm}{$xos}{$h}) {
          $flag = 0;
          push @missing_hosts,$h;
          $z_host_rpm_missing{$h}{$rpm} = 1;
          #print "[$group][$h][$rpm][@zallhosts]\n" if $h eq "titan06" && $group eq "titan";
        }
      }

      if ($flag) {
        push @rpm_common,"$rpm: ".$inst_hosts;
        $z_os_rpm_common{$xos}{$rpm} = 1;
      } else {
        if (scalar @inst_hosts < scalar @missing_hosts) {
          push @rpm_extra,"$rpm: ".$inst_hosts;
        } else {
          push @rpm_missing,"$rpm missing on: ".join(" ",@missing_hosts);
        }
      }

      #print "$rpm: [$inst_hosts] [$zallhosts]\n";
    }

    $qrpms_common_label{$xos} = writeFile("byOS/".$os."_packages_common_".$group.".txt",join("\n",@rpm_common),scalar @rpm_common);

    $html .= htmltag("td","align=right",scalar keys %{$zhosts{$xos}});
    $html .= htmltag("td","align=right",writeFile("byOS/".$xos."_packages_".$group.".txt",join("\n",@rpm_all),scalar @rpm_all));
    $html .= htmltag("td","align=right",$qrpms_common_label{$xos});
    $html .= htmltag("td","align=right",writeFile("byOS/".$xos."_packages_extra_".$group.".txt",join("\n",@rpm_extra),scalar @rpm_extra));
    $html .= htmltag("td","align=right",writeFile("byOS/".$xos."_packages_missing_".$group.".txt",join("\n",@rpm_missing),scalar @rpm_missing));

    $html .= "</tr>\n";
  }

  $html .= "</table>\n";

  $html .= htmltag("h2","","Packages by host")."\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";

  $html .= "<tr>";
  $html .= htmltag("th","","Hostname");
  $html .= htmltag("th","","OS");
  $html .= htmltag("th","","Installed");
  $html .= htmltag("th","","Common");
  $html .= htmltag("th","","Extra");
  $html .= htmltag("th","","Missing");
  $html .= "</tr>\n";

  foreach my $xos (sort keys %zos) {
    my $os = $zos{$xos};
    foreach my $h (sort keys %{$zhosts{$xos}}) {
      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","",$h);
      $html .= htmltag("td","",$os);

      my @rpm = sort keys %{$z_host_rpm{$h}};

      if (scalar @rpm < 1) {
        $html .= htmltag("td","align=right","0");
        $html .= htmltag("td","align=right","?");
        $html .= htmltag("td","align=right","?");
        $html .= htmltag("td","align=right","?");
        $html .= "</tr>\n";
        next;
      }

      my @rpmextra;
      #my @rpmmissing = sort keys %{$z_host_rpm_missing{$h}};

      my @rpmmissing;
      foreach my $rpm (sort keys %{$z_host_rpm_missing{$h}}) {
        my @onhosts = keys %{$z_rpm_os_host{$rpm}{$xos}};
        my $onhosts = join(" ", @onhosts);
        #die "$h [$rpm installed on: $onhosts]\n";
        push @rpmmissing, "$rpm installed on: $onhosts";
      }

      foreach my $rpm (@rpm) {
        push @rpmextra, $rpm if ! defined $z_os_rpm_common{$xos}{$rpm};
      }

      $html .= htmltag("td","align=right",writeFile("byHost/".$h."_packages_".$group.".txt",join("\n",@rpm),scalar @rpm));
      $html .= htmltag("td","align=right",$qrpms_common_label{$xos});
      $html .= htmltag("td","align=right",writeFile("byHost/".$h."_packages_extra_".$group.".txt",join("\n",@rpmextra),scalar @rpmextra));
      $html .= htmltag("td","align=right",writeFile("byHost/".$h."_packages_missing_".$group.".txt",join("\n",@rpmmissing),scalar @rpmmissing));

      $html .= "</tr>\n";
    }
  }

  $html .= "</table>\n";

  return $html;
}

sub genAlarms
{
  my $group = shift @_;
  my $html;

  $html .= htmltag("h2","","Alarms")."\n";

  $html .= "<table border=1 width=\"90%\" align=center>\n";
  
  $html .= "<tr>";
  $html .= htmltag("th","","Hostname");
  $html .= htmltag("th","","Alarms");
  $html .= htmltag("th","","Email");
  $html .= "</tr>\n";
  
  foreach my $h (sort keys %alarms)
    {
      my $class = $class{$h};
      $class = "" if ! defined $class;

      print "genAlarms: [$group] [$h] [$class] [$alarms{$h}]\n" if $verbose;

      #$html .= "<a name='alarms_$h'></a>";
      $html .= "<tr class=$class>";
      $html .= htmltag("td","align=center",$h);
      
      my %a;
      foreach my $a (sort split/\s+/, $alarms{$h})
        {
          $a{$a} = 1;
        }
      
      my $aaa = join(" ", sort keys %a);
      
      $html .= htmltag("td","align=left",$aaa);

      my $bbb = "";
      if ($email{$h}) {
        my %b;
        foreach my $b (sort split/\s+/, $email{$h})
          {
            $b{$b} = 1;
          }
        
        $bbb = join(" ", sort keys %b);
      }
      
      $html .= htmltag("td","align=left",$bbb);
      
      $html .= "</tr>";
    }
  
  $html .= "</table>\n";

  return $html;
}

sub genMonitoring
{
  my $group = shift @_;
  my $html;

  $html .= "<h2>Configuration of monitoring scripts</h2>\n";

  $html .= "<table border=1 width=\"90%\" align=center class=sortable>\n";
  $html .= "<tr>";
  $html .= htmltag("td","align=center",htmltag("b","","Hostname"));
  $html .= htmltag("td","align=center",htmltag("b","","nodeinfo"));
  $html .= htmltag("td","align=center",htmltag("b","","diskscrub"));
  $html .= htmltag("td","align=center",htmltag("b","","ganglia"));
  $html .= htmltag("td","align=center",htmltag("b","","ganglia<br>sensors"));
  $html .= htmltag("td","align=center",htmltag("b","","ganglia<br>smart"));
  $html .= htmltag("td","align=center",htmltag("b","","ganglia<br>top"));
  $html .= htmltag("td","align=center",htmltag("b","","ganglia<br>vmstat"));
  $html .= "</tr>\n";

  foreach my $h (sort keys %jsondata) {
    next unless $jsondata{$h}{"groups"}{$group};
    my @rpms = @{$jsondata{$h}{"split_rpms"}};
      
      my $nodeinfo = "";
      foreach $r (sort grep(/triumf_nodeinfo/, @rpms))
        {
          $r =~ s/triumf_nodeinfo-//;
          $r =~ s/.noarch//;
          $nodeinfo .= "$r ";
        }
      
      my $diskscrub = "";
      foreach $r (sort grep(/diskscrub/, @rpms))
        {
          $r =~ s/diskscrub-//;
          $r =~ s/.noarch//;
          $diskscrub .= "$r ";
        }
      
      my $ganglia = "";
      foreach $r (sort grep(/ganglia/, @rpms))
        {
          next unless $r =~ /$ganglia/;
          $r =~ s/ganglia-gmond-//;
          $r =~ s/ganglia-//;
          $r =~ s/.noarch//;
          $r =~ s/.x86_64//;
          $r =~ s/.i386//;
          $r =~ s/.i686//;
          $r =~ s/.el\d+//;
          $ganglia .= "$r ";
        }
      
      my $ganglia_sensors = "";
      foreach $r (sort grep(/ganglia-sensors/, @rpms))
        {
          $r =~ s/ganglia-sensors-//;
          $r =~ s/.noarch//;
          $ganglia_sensors .= "$r ";
        }
      
      my $ganglia_smart = "";
      foreach $r (sort grep(/ganglia-smart/, @rpms))
        {
          $r =~ s/ganglia-smart-//;
          $r =~ s/.noarch//;
          $ganglia_smart .= "$r ";
        }
      
      my $ganglia_top = "";
      foreach $r (sort grep(/ganglia-top/, @rpms))
        {
          $r =~ s/ganglia-top-//;
          $r =~ s/.noarch//;
          $ganglia_top .= "$r ";
        }
      
      my $ganglia_vmstat = "";
      foreach $r (sort grep(/ganglia-vmstat/, @rpms))
        {
          $r =~ s/ganglia-vmstat-//;
          $r =~ s/.noarch//;
          $ganglia_vmstat .= "$r ";
        }
      
      $html .= "<tr class=$class{$h}>";
      $html .= htmltag("td","align=center",$h);
      $html .= htmltag("td", "align=center", $nodeinfo);
      $html .= htmltag("td", "align=center", $diskscrub);
      $html .= htmltag("td", "align=center", $ganglia);
      $html .= htmltag("td", "align=center", $ganglia_sensors);
      $html .= htmltag("td", "align=center", $ganglia_smart);
      $html .= htmltag("td", "align=center", $ganglia_top);
      $html .= htmltag("td", "align=center", $ganglia_vmstat);
      $html .= "</tr>\n";
    }
  
  $html .= "</table>\n";

  return $html;
}

sub statusColour
  {
    my $status = shift @_;
    return "bgcolor=red"    if $status > 1;
    return "bgcolor=yellow" if $status > 0;
    return "";
  }

sub setStatus
  {
    my $host = shift @_;
    my $code = shift @_;
    my $name = shift @_;
    my $mail = shift @_;

    if ($code > 0) {
      print "Set status: [$host] [$code] [$name] [$email]\n" if $verbose;
    }

    $host_status{$host} = 0 if ! defined $host_status{$host};

    if ($code > $host_status{$host}) {
      $host_status{$host} = $code;
      $classmain{$host} = "yellow" if $code > 0;
      $classmain{$host} = "red"    if $code > 1;
    }

    if ($code > 0) {
      if (! defined $host_alarms{$host}{$name}) {
        $host_alarms{$host}{$name} = 1;
        $alarms{$host} .= "$name ";
      }
    }

    if ($mail && $code > 1 && !$stale{$host}) {
      if (! defined $host_email{$host}{$name}) {
        $host_email{$host}{$name} = 1;
        $email{$host} .= "$name ";
      }
    }
  }

sub findXmlTag
  {
    my $data = shift @_;
    my $name = shift @_;
    return "???NOXML???" unless $data;
    return unless $data =~ /<$name>(.*?)<\/$name>/s;
    return $1;
  }

sub splitXmlTag
  {
    my $data = shift @_;
    my $name = shift @_;
    my @r;
    while (1) {
      my ($s, $tail) = ($data =~ /<$name>(.*?)<\/$name>(.*)$/s);
      #print "split [$name][$data][$s][$tail]\n";
      last if (!$s);
      $data = $tail;
      push @r, $s;
    }
    return @r;
  }

sub SaveArr
  {
    my $file = shift @_;
    my $dataref = shift @_;
    $dataref = [] if ! defined $dataref;
    open (TMPOUT,">$confWeb/$file");
    print TMPOUT join("\n",@$dataref);
    close TMPOUT;
    return "<a href=\"$file\">".(scalar @$dataref)."</a>";
  }

sub htmltag
  {
    my $tag    = shift @_;
    my $params = shift @_;
    my @contents = @_;
    @contents = () unless defined $contents[0];
    my $content = join("",@contents);
    return "<$tag $params>$content</$tag>";
  }

sub readFile
  {
    my $filename = shift @_;
    #warn "Cannot read file $filename: $!" if ! -r $filename;
    #my $data = `cat $filename`;
    #return $data;
    print "Reading file: $filename\n" if $verbose;
    if (open (TMPIN,$filename))
      {
	my $data = join("",<TMPIN>);
	close TMPIN;
	return $data;
      }
    else
      {
	warn "Cannot read file $filename: $!";
	return "";
      }
  }

sub writeFile
  {
    my $filename = shift @_;
    my $data     = shift @_;
    my $label    = shift @_;
    my $file     = "$confWeb/$filename";
    open (TMPOUT,">$file") || die "Cannot write $file: $!";
    print TMPOUT $data;
    close TMPOUT;
    #print STDERR "Write file $filename\n";
    if (length($data)<1)
      {
	return $label;
      }
    else
      {
	return htmltag("a","href=\"$filename\"",$label);
      }
  }

sub makeref
  {
    return \@_;
  }


sub showAll
  {
    my $allhosts = shift @_;
    my $reqhosts = shift @_;
    my $installeditems = shift @_;
    my $requireditems  = shift @_;
    my $data = shift @_;
    my %dupe;

    my $requireditemshash = listtohash(@{$requireditems});

    my $outall = "";
    my $outallcount = 0;
    my $outmissing = "";
    my $outmissingcount = 0;
    my $outextra = "";
    my $outextracount = 0;

    foreach my $item (sort @{$installeditems})
      {
	next if defined $dupe{$item};

	my %dupehost;

	foreach my $h (@{$$data{$item}{"hosts"}})
	  {
	    next if (! grep(/$h/,@{$allhosts}));
	    next if defined $dupehost{$h};
	    $dupehost{$h} = 1;
	  }

	$dupe{$item} = \%dupehost;

	my @xhosts = keys %dupehost;

	$outall .= $item .": ".join(", ",@xhosts)."\n";
	$outallcount++;

	if (defined $$requireditemshash{$item})
	  {
	  }
	else
	  {
	    $outextra .= $item .": ".join(", ",@xhosts)."\n";
	    $outextracount++;
	  }
      }

    foreach my $item (sort @{$requireditems})
      {
	my @missing;
	my $xhosts = $dupe{$item};

	#print "item=$item, xhosts=",$xhosts,"\n";

	foreach my $rh (sort @{$reqhosts})
	  {
	    push @missing,$rh if ! defined $$xhosts{$rh};
	  }
	
	if (@missing > 0)
	  {
	    $outmissing .= $item ." not on: ".join(", ",@missing)."\n";
	    $outmissingcount++;
	  }

	#die "Here!";
      }

    return ($outall,$outallcount,$outmissing,$outmissingcount,$outextra,$outextracount);
  }

sub listtohash
  {
    my %xhash;
    foreach my $v (@_)
      {
	$xhash{$v} = 1;
      }
    return \%xhash;
  }

sub splitRpm
  {
    my $rpm = shift @_;
    $rpm =~ /(.*)-(.*-.*)/;
    my $base    = $1;
    my $version = $2;
    return ($base, $version);
  }

#sub readRpms
#  {
#    my $file = shift @_;
#    my %rpms;
#    foreach my $rpm (sort split(/\n/,$file))
#      {
#	my ($rpmbase,$rpmversion) = splitRpm($rpm);
#	#print "Package: $rpm: $rpmbase, $rpmversion\n";
#	push @{$rpms{$rpmbase}},$rpmversion;
#      }
#    return \%rpms;
#  }

sub vercmp
  {
    my @ver1 = split(/[-\.]/,shift @_);
    my @ver2 = split(/[-\.]/,shift @_);

    #print "ver1: ".join("=",@ver1)."\n";
    #print "ver2: ".join("=",@ver2)."\n";

    for(my $i=0; ; $i++)
      {
	return -1 if !defined $ver1[$i];
	return +1 if !defined $ver2[$i];
	if (($ver1[$i]=~/^\d+$/)&&($ver2[$i]=~/^\d+$/))
	  {
	    my $cmp = $ver1[$i] <=> $ver2[$i];
	    #print "$i $cmp\n";
	    return $cmp if $cmp != 0;
	  }
	else
	  {
	    my $cmp = $ver1[$i] cmp $ver2[$i];
	    #print "$ver1[$i] $ver2[$i] $i $cmp\n";
	    return $cmp if $cmp != 0;
	  }
      }

    die "Error in vercmp!";
  }

#end file
