# README #

The gonodeinfo package is a management tool for clusters of computers.

Configuration data from computers running the **gonodeinfo** agent is collected to a computer running the **gonodereceive** server. The **gonodereport** program generates web reports and email alarms (as configured).

Data is collected using the JSON data format, all programs are written using the GO programming language. A previous incarnation of this tool was written in perl and used the XML data format.

Here is an [example](example.html) web report page.

### To try without installing ###

    go version # check that the "go" compiler is installed
    #yum install golang # install "go" if needed
    #apt-get install golang # install "go" if needed
    git clone https://bitbucket.org/dd1/gonodeinfo.git
    cd gonodeinfo
    make
    ./gonodereceive.exe -v -e & # run in the background
    ./gonodeinfo.exe -v -e
    # observe connection from unknown client is rejected
    # follow instructions printed by gonodereceive to authorize this client
    # touch localhost.json
    # and run the agent again
    ./gonodeinfo.exe -v -e
    ls -ltr *.json # observe json file is created or updated
    ./gonodereport.exe -v -e
    ls -l gonodereport.html # observe report file is created or updated
    firefox gonodereport.html # open the report file

### To install for production use ###

    su - # become root
    go version # check that the "go" compiler is installed
    #yum install golang # install "go" if needed
    cd ~root # go to the root's home directory
    git clone https://bitbucket.org/dd1/gonodeinfo.git
    cd gonodeinfo
    make
    make install # install gonodeinfo agent
    #make server  # install server components - gonodereceive and gonodereport

### To configure and test production server ###

* open a root shell on the server
* follow above instructions to install the server components (run "make server")
* examine /etc/gonodereport.conf
* cd ~ # wrong config files are used if run from the gonodeinfo directory
* /opt/gonodeinfo/gonodereceive.exe & # run in the background
* follow instructions below to add the first client agent
* ls -ltr /opt/gonodeinfo/data # check that new data was received
* /opt/gonodeinfo/gonodereport.exe -v -e # generate report
* ls -l /var/www/html/gonodeinfo # check that new report was generated
* firefox http://localhost/gonodeinfo/ # open the web report

### To add a new client agent ###

* open a root shell on the client (root shell on the server is needed, too)
* follow above instructions to install the agent components (do NOT run "make server")
* cd /
* examine and edit /etc/gonodeinfo.conf:
* change Description as desired
* change Servers from localhost to the full hostname of the server
* cd /
* /opt/gonodeinfo/gonodeinfo.exe
* if error is "connection refused", gonodereceive.exe is not running or is blocked by firewall rules
* On the server: gonodereceive will reject connection from unknown client and print instructions on how to enable it: i.e. touch /opt/gonodeinfo/data/142.90.111.176.json
* On the server: run the command to permit the new client
* On the client: run gonodeinfo.exe again
* On the server: check for no rejection message, "ls -ltr /opt/gonodeinfo/data" to see that new data was received.
* On the server: run gonodereport.exe to regenerate the web report