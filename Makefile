# Makefile

EXES += gonodeinfo.exe
#EXES += gonodereport.exe
EXES += gonodereceive.exe

FILES += gonodeinfo.exe
FILES += gonodeinfo.go
FILES += gonodeinfo.conf
FILES += gonodeinfo.cron

DIR := /opt/gonodeinfo

all:: $(EXES)

gofmt:
	gofmt -w gonodeinfo.go
	gofmt -w gonodereceive.go
	gofmt -w gonodereport.go

install: gonodeinfo.exe
	mkdir -p $(DIR)
	cp -v gonodeinfo.exe  $(DIR)
	cp -v gonodeinfo.go   $(DIR)
	cp -v -n gonodeinfo.cron $(DIR)
	cp -v -n gonodeinfo.conf /etc
	-ln -v -s $(DIR)/gonodeinfo.cron /etc/cron.d/gonodeinfo
	-ln -v -s $(DIR)/gonodeinfo.exe  /bin/gonodeinfo
	touch /etc/crontab

server: all
	mkdir -p $(DIR)
	mkdir -p $(DIR)/data
	-cp -v gonodereceive.exe    $(DIR)
	cp -v gonodereceive.go     $(DIR)
	#cp -v gonodereport.exe     $(DIR)
	#cp -v gonodereport.go      $(DIR)
	cp -v gonodereport.perl    $(DIR)
	cp -n -v gonodereport.conf $(DIR)/gonodereport.conf
	cp -n -v gonodereport.cron    $(DIR)
	-ln -v -s $(DIR)/gonodereport.conf /etc
	-ln -v -s $(DIR)/gonodereport.cron /etc/cron.d/gonodereport
	#-ln -v -s $(DIR)/gonodereport.exe  /bin/gonodereport
	ln -v -s -f $(DIR)/gonodereport.perl  /bin/gonodereport
	#-ln -v -s $(DIR)/gonodereceive.exe  /bin/gonodereceive
	touch /etc/crontab
	mkdir -p /var/www/html/gonodeinfo
	cp -v sorttable.js /var/www/html/gonodeinfo
	#/opt/gonodeinfo/gonodereport.exe -c /opt/gonodeinfo/gonodereport.conf
	/bin/gonodereport

clean::
	-/bin/rm -vf *.exe
	-/bin/rm -vf $(EXES)

%.exe: %.go; go build -o $@ $<

#end
